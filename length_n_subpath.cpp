//计算 整个CFG的length_n_path数量
#include "length_n_subpath.h"  
#include "llvm/Pass.h" 
#include "llvm/ADT/Twine.h"
#include "mylib/printSTL.h" 
#include <string>
#include <queue>
#include <set>  
#include <fstream>  
#include <sstream>  


using namespace llvm;
using namespace std;
//init depth is 1


extern int  subpathLength ;

 
std::vector<CFGNode*> pathStack; 
set<deque<CFGNode*> > subpathSet;
 
void findAllSubpath(CFGNode *node,int pathBegin,int pathLength);
bool  saveSubpath(int first ,int last);


int totalSubpath(ProgramCFG &cfg,int  n){
	//bfs(*M.getFunction("g"));
	string errorInfo;
	int numOfSubpath = -1;


	subpathLength = n;
	CFGNode *root = cfg.getRoot();   

	for(int i = 1; i <= subpathLength; i++){
		pathStack.clear();
		subpathSet.clear();
		pathStack.push_back(root); 
 	findAllSubpath(root,0,i);

 	std::error_code ec;
 	stringstream fileName;
 	fileName<<"length_" << i<<"subpath"; 
		ofstream outputStream (fileName.str().c_str());

	 numOfSubpath = subpathSet.size();
		errs() << "\nnumber of length-" << i << " subpath is " << numOfSubpath << "\n\n";
		outputStream<< i << "\n";
		outputStream << numOfSubpath << "\n";
		outputStream << "length-"<<i<<" subpath coverage is " << 0 << "\n"; //length n subpath coverage
		outputStream << subpathSet;
		outputStream.close();
	}
	return numOfSubpath;
}	
 
void findAllSubpath(CFGNode *node,int pathBegin,int pathLength){
	bool findNewSubpath = false;
	//errs() << "findAllSubpath: node "<<node << "\n";
	//errs()<<"size = " << subpathSet.size()<<"pathbegin = "<<pathBegin<<" length = "<<pathStack.size() - pathBegin<<"\n";

	if(pathStack.size() - pathBegin  == pathLength){
		//if current subpath from pathbegin to end is already saved,return.
		if(!saveSubpath(pathBegin,pathStack.size()-1))
			return ;

		//remove the first one
		pathBegin++;
		findNewSubpath = true;
	}

	Succ* succ = node->getFirstSucc(); 
	if(!succ){
		//Now,node is the last node of one complete path.
		//if we don't find newSubpath, indicates that the complete path length is shorter than subpathLength.
		//We also save it.
		if(!findNewSubpath)
			saveSubpath(pathBegin,pathStack.size()-1);
		return ;
	}
	//node has at least one success.
	while(succ){
		pathStack.push_back(succ->v);
		//If the subPath doesn't exists,travel the succssor.
		findAllSubpath(succ->v,pathBegin,pathLength);
		//errs()<<"back!!!!!!!!!!!!!!\n";
		pathStack.pop_back(); 
		succ  = succ->nextSucc;
	}

}

 

bool  saveSubpath(int first ,int last){
	deque<CFGNode*> newSubpath;
	int i = 0;
	while(first <= last){
		newSubpath.push_back(pathStack[first]);
		first++;
		i++;
		//errs() << i<<" ";
	}
	return subpathSet.insert(newSubpath).second;
} 

 
 