#ifndef FINDLOOP_H
#define FINDLOOP_H
#include "llvm/IR/BasicBlock.h"
#include "programCFG.h"

using namespace llvm;

void findAllBackedges(Module &M,ProgramCFG& pCFG);

void findAllScc(Module &M,ProgramCFG& pCFG);
#endif