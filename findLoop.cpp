
#include "llvm/IR/CFG.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/IR/Module.h" 
#include "llvm/Support/raw_ostream.h" 
#include "llvm/Support/FileSystem.h"
#include "findLoop.h"
#include "llvm/ADT/SCCIterator.h"
#include "llvm/ADT/GraphTraits.h"
#include "llvm/Analysis/LoopInfo.h"
void findAllBackedges(Module &M,ProgramCFG& pCFG){
	std:: string errorInfo ;
	raw_fd_ostream File("CFG.BE",errorInfo,sys::fs::F_Text); 
	if(!errorInfo.empty()){
		errs() << "can't write file CFG.BE\n";
		exit(-1); 
	}

	SmallVector<std::pair<const BasicBlock *, const BasicBlock *>,100> 
	        backedges;
	for(Module::iterator fi = M.begin();fi != M.end(); fi++){
		if(!fi->isDeclaration()){
			//find back edges,save in the backedges
			FindFunctionBackedges(*fi,backedges);  
		}
	}

	//save backedges and loop exits to file.
	for(SmallVector<std::pair<const BasicBlock *, const BasicBlock *>,100>::iterator it = backedges.begin(); it != backedges.end(); it++){
			int from = pCFG.getNode((BasicBlock*)&*(it->first))->id;
			int to  = pCFG.getNode((BasicBlock*)&*(it->second))->id;
			File<<from <<" "<<to<<"\n";

			//find loop exit.Given a backage <a,b> in loop L, loop exits are the
			//successors of b which are outside of L.  
			
	}
}

void findAllScc(Module &M,ProgramCFG& pCFG){
	// Use LLVM's Strongly Connected Components (SCCs) iterator to produce
	// a reverse topological sort of SCCs.
	for(Module::iterator fi = M.begin(); fi != M.end(); fi++){
		if(fi->isDeclaration()) continue;
		errs() << "SCCs for " << fi->getName() << " in post-order:\n";
		for (scc_iterator<Function *> I = scc_begin(&*fi),
	                              IE = scc_end(&*fi);
	                              I != IE; ++I) {
	  // Obtain the vector of BBs in this SCC and print it out.
	  const std::vector<BasicBlock *> &SCCBBs = *I;
	  errs() << "  SCC: ";
	  for (std::vector<BasicBlock *>::const_iterator BBI = SCCBBs.begin(),
	                                                 BBIE = SCCBBs.end();
	                                                 BBI != BBIE; ++BBI) {
	  		int id = pCFG.getNode((*BBI))->id;
	    errs() << id << "  ";
	  }
	  errs() << "\n";
		}	
	}
}