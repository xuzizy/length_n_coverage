//计算 整个CFG的length_n_path数量
#ifndef LENGTH_N_SUBPATH
#define LENGTH_N_SUBPATH
#include "llvm/IR/BasicBlock.h"
#include "llvm/Analysis/CFG.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/Module.h" 
#include "programCFG.h"

using namespace llvm;

int totalSubpath(ProgramCFG &cfg,int  n);
#endif

