; ModuleID = 'tcaslength_n_coverage.bc'
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i386-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i32, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i32, i32, [40 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@Positive_RA_Alt_Thresh = common global [4 x i32] zeroinitializer, align 4
@Alt_Layer_Value = common global i32 0, align 4
@Climb_Inhibit = common global i32 0, align 4
@Up_Separation = common global i32 0, align 4
@Down_Separation = common global i32 0, align 4
@Cur_Vertical_Sep = common global i32 0, align 4
@Own_Tracked_Alt = common global i32 0, align 4
@Other_Tracked_Alt = common global i32 0, align 4
@High_Confidence = common global i32 0, align 4
@Own_Tracked_Alt_Rate = common global i32 0, align 4
@Other_Capability = common global i32 0, align 4
@Two_of_Three_Reports_Valid = common global i32 0, align 4
@Other_RAC = common global i32 0, align 4
@stdout = external global %struct._IO_FILE*
@.str = private unnamed_addr constant [35 x i8] c"Error: Command line arguments are\0A\00", align 1
@.str1 = private unnamed_addr constant [63 x i8] c"Cur_Vertical_Sep, High_Confidence, Two_of_Three_Reports_Valid\0A\00", align 1
@.str2 = private unnamed_addr constant [58 x i8] c"Own_Tracked_Alt, Own_Tracked_Alt_Rate, Other_Tracked_Alt\0A\00", align 1
@.str3 = private unnamed_addr constant [49 x i8] c"Alt_Layer_Value, Up_Separation, Down_Separation\0A\00", align 1
@.str4 = private unnamed_addr constant [44 x i8] c"Other_RAC, Other_Capability, Climb_Inhibit\0A\00", align 1
@.str5 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1

; Function Attrs: nounwind
define void @initialize() #0 {
entry:
  store i32 400, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 0), align 4, !dbg !47
  store i32 500, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 1), align 4, !dbg !48
  store i32 640, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 2), align 4, !dbg !49
  store i32 740, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 3), align 4, !dbg !50
  %0 = call i32 @recordBasicBlock(i32 0, i32 8)
  ret void, !dbg !51
}

; Function Attrs: nounwind
define i32 @ALIM() #0 {
entry:
  %0 = load i32* @Alt_Layer_Value, align 4, !dbg !52
  %arrayidx = getelementptr inbounds [4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 %0, !dbg !52
  %1 = load i32* %arrayidx, align 4, !dbg !52
  %2 = call i32 @recordBasicBlock(i32 1, i32 8)
  ret i32 %1, !dbg !52
}

; Function Attrs: nounwind
define i32 @Inhibit_Biased_Climb() #0 {
entry:
  %0 = load i32* @Climb_Inhibit, align 4, !dbg !53
  %tobool = icmp ne i32 %0, 0, !dbg !53
  %1 = call i32 @recordBasicBlock(i32 2, i32 8)
  br i1 %tobool, label %cond.true, label %cond.false, !dbg !53

cond.true:                                        ; preds = %entry
  %2 = load i32* @Up_Separation, align 4, !dbg !54
  %add = add nsw i32 %2, 100, !dbg !54
  %3 = call i32 @recordBasicBlock(i32 3, i32 8)
  br label %cond.end, !dbg !54

cond.false:                                       ; preds = %entry
  %4 = load i32* @Up_Separation, align 4, !dbg !56
  %5 = call i32 @recordBasicBlock(i32 4, i32 8)
  br label %cond.end, !dbg !56

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ %4, %cond.false ], !dbg !53
  %6 = call i32 @recordBasicBlock(i32 5, i32 8)
  ret i32 %cond, !dbg !58
}

; Function Attrs: nounwind
define i32 @Non_Crossing_Biased_Climb() #0 {
entry:
  %upward_preferred = alloca i32, align 4
  %upward_crossing_situation = alloca i32, align 4
  %result = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %upward_preferred}, metadata !61), !dbg !62
  call void @llvm.dbg.declare(metadata !{i32* %upward_crossing_situation}, metadata !63), !dbg !64
  call void @llvm.dbg.declare(metadata !{i32* %result}, metadata !65), !dbg !66
  %call = call i32 @Inhibit_Biased_Climb(), !dbg !67
  %0 = load i32* @Down_Separation, align 4, !dbg !67
  %cmp = icmp sgt i32 %call, %0, !dbg !67
  %conv = zext i1 %cmp to i32, !dbg !67
  store i32 %conv, i32* %upward_preferred, align 4, !dbg !67
  %1 = load i32* %upward_preferred, align 4, !dbg !68
  %tobool = icmp ne i32 %1, 0, !dbg !68
  %2 = call i32 @recordBasicBlock(i32 6, i32 8)
  br i1 %tobool, label %if.then, label %if.else, !dbg !68

if.then:                                          ; preds = %entry
  %call1 = call i32 @Own_Below_Threat(), !dbg !70
  %tobool2 = icmp ne i32 %call1, 0, !dbg !70
  %3 = call i32 @recordBasicBlock(i32 7, i32 8)
  br i1 %tobool2, label %lor.rhs, label %lor.end, !dbg !70

lor.rhs:                                          ; preds = %if.then
  %call3 = call i32 @Own_Below_Threat(), !dbg !72
  %tobool4 = icmp ne i32 %call3, 0, !dbg !72
  %4 = call i32 @recordBasicBlock(i32 8, i32 8)
  br i1 %tobool4, label %land.rhs, label %land.end, !dbg !72

land.rhs:                                         ; preds = %lor.rhs
  %5 = load i32* @Down_Separation, align 4, !dbg !74
  %call5 = call i32 @ALIM(), !dbg !76
  %cmp6 = icmp sge i32 %5, %call5, !dbg !76
  %lnot = xor i1 %cmp6, true, !dbg !76
  %6 = call i32 @recordBasicBlock(i32 9, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %7 = phi i1 [ false, %lor.rhs ], [ %lnot, %land.rhs ]
  %8 = call i32 @recordBasicBlock(i32 10, i32 8)
  br label %lor.end, !dbg !77

lor.end:                                          ; preds = %land.end, %if.then
  %9 = phi i1 [ true, %if.then ], [ %7, %land.end ]
  %lor.ext = zext i1 %9 to i32, !dbg !79
  store i32 %lor.ext, i32* %result, align 4, !dbg !79
  %10 = call i32 @recordBasicBlock(i32 11, i32 8)
  br label %if.end, !dbg !82

if.else:                                          ; preds = %entry
  %call8 = call i32 @Own_Above_Threat(), !dbg !83
  %tobool9 = icmp ne i32 %call8, 0, !dbg !83
  %11 = call i32 @recordBasicBlock(i32 12, i32 8)
  br i1 %tobool9, label %land.lhs.true, label %land.end16, !dbg !83

land.lhs.true:                                    ; preds = %if.else
  %12 = load i32* @Cur_Vertical_Sep, align 4, !dbg !85
  %cmp10 = icmp sge i32 %12, 300, !dbg !85
  %13 = call i32 @recordBasicBlock(i32 13, i32 8)
  br i1 %cmp10, label %land.rhs12, label %land.end16, !dbg !85

land.rhs12:                                       ; preds = %land.lhs.true
  %14 = load i32* @Up_Separation, align 4, !dbg !87
  %call13 = call i32 @ALIM(), !dbg !89
  %cmp14 = icmp sge i32 %14, %call13, !dbg !89
  %15 = call i32 @recordBasicBlock(i32 14, i32 8)
  br label %land.end16

land.end16:                                       ; preds = %land.rhs12, %land.lhs.true, %if.else
  %16 = phi i1 [ false, %land.lhs.true ], [ false, %if.else ], [ %cmp14, %land.rhs12 ]
  %land.ext = zext i1 %16 to i32, !dbg !90
  store i32 %land.ext, i32* %result, align 4, !dbg !90
  %17 = call i32 @recordBasicBlock(i32 15, i32 8)
  br label %if.end

if.end:                                           ; preds = %land.end16, %lor.end
  %18 = load i32* %result, align 4, !dbg !93
  %19 = call i32 @recordBasicBlock(i32 16, i32 8)
  ret i32 %18, !dbg !93
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: nounwind
define i32 @Non_Crossing_Biased_Descend() #0 {
entry:
  %upward_preferred = alloca i32, align 4
  %upward_crossing_situation = alloca i32, align 4
  %result = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %upward_preferred}, metadata !94), !dbg !95
  call void @llvm.dbg.declare(metadata !{i32* %upward_crossing_situation}, metadata !96), !dbg !97
  call void @llvm.dbg.declare(metadata !{i32* %result}, metadata !98), !dbg !99
  %call = call i32 @Inhibit_Biased_Climb(), !dbg !100
  %0 = load i32* @Down_Separation, align 4, !dbg !100
  %cmp = icmp sgt i32 %call, %0, !dbg !100
  %conv = zext i1 %cmp to i32, !dbg !100
  store i32 %conv, i32* %upward_preferred, align 4, !dbg !100
  %1 = load i32* %upward_preferred, align 4, !dbg !101
  %tobool = icmp ne i32 %1, 0, !dbg !101
  %2 = call i32 @recordBasicBlock(i32 17, i32 8)
  br i1 %tobool, label %if.then, label %if.else, !dbg !101

if.then:                                          ; preds = %entry
  %call1 = call i32 @Own_Below_Threat(), !dbg !103
  %tobool2 = icmp ne i32 %call1, 0, !dbg !103
  %3 = call i32 @recordBasicBlock(i32 18, i32 8)
  br i1 %tobool2, label %land.lhs.true, label %land.end, !dbg !103

land.lhs.true:                                    ; preds = %if.then
  %4 = load i32* @Cur_Vertical_Sep, align 4, !dbg !105
  %cmp3 = icmp sge i32 %4, 300, !dbg !105
  %5 = call i32 @recordBasicBlock(i32 19, i32 8)
  br i1 %cmp3, label %land.rhs, label %land.end, !dbg !105

land.rhs:                                         ; preds = %land.lhs.true
  %6 = load i32* @Down_Separation, align 4, !dbg !107
  %call5 = call i32 @ALIM(), !dbg !109
  %cmp6 = icmp sge i32 %6, %call5, !dbg !109
  %7 = call i32 @recordBasicBlock(i32 20, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %if.then
  %8 = phi i1 [ false, %land.lhs.true ], [ false, %if.then ], [ %cmp6, %land.rhs ]
  %land.ext = zext i1 %8 to i32, !dbg !110
  store i32 %land.ext, i32* %result, align 4, !dbg !110
  %9 = call i32 @recordBasicBlock(i32 21, i32 8)
  br label %if.end, !dbg !113

if.else:                                          ; preds = %entry
  %call8 = call i32 @Own_Above_Threat(), !dbg !114
  %tobool9 = icmp ne i32 %call8, 0, !dbg !114
  %10 = call i32 @recordBasicBlock(i32 22, i32 8)
  br i1 %tobool9, label %lor.rhs, label %lor.end, !dbg !114

lor.rhs:                                          ; preds = %if.else
  %call10 = call i32 @Own_Above_Threat(), !dbg !116
  %tobool11 = icmp ne i32 %call10, 0, !dbg !116
  %11 = call i32 @recordBasicBlock(i32 23, i32 8)
  br i1 %tobool11, label %land.rhs12, label %land.end16, !dbg !116

land.rhs12:                                       ; preds = %lor.rhs
  %12 = load i32* @Up_Separation, align 4, !dbg !118
  %call13 = call i32 @ALIM(), !dbg !120
  %cmp14 = icmp sge i32 %12, %call13, !dbg !120
  %13 = call i32 @recordBasicBlock(i32 24, i32 8)
  br label %land.end16

land.end16:                                       ; preds = %land.rhs12, %lor.rhs
  %14 = phi i1 [ false, %lor.rhs ], [ %cmp14, %land.rhs12 ]
  %15 = call i32 @recordBasicBlock(i32 25, i32 8)
  br label %lor.end, !dbg !121

lor.end:                                          ; preds = %land.end16, %if.else
  %16 = phi i1 [ true, %if.else ], [ %14, %land.end16 ]
  %lor.ext = zext i1 %16 to i32, !dbg !123
  store i32 %lor.ext, i32* %result, align 4, !dbg !123
  %17 = call i32 @recordBasicBlock(i32 26, i32 8)
  br label %if.end

if.end:                                           ; preds = %lor.end, %land.end
  %18 = load i32* %result, align 4, !dbg !126
  %19 = call i32 @recordBasicBlock(i32 27, i32 8)
  ret i32 %18, !dbg !126
}

; Function Attrs: nounwind
define i32 @Own_Below_Threat() #0 {
entry:
  %0 = load i32* @Own_Tracked_Alt, align 4, !dbg !127
  %1 = load i32* @Other_Tracked_Alt, align 4, !dbg !127
  %cmp = icmp slt i32 %0, %1, !dbg !127
  %conv = zext i1 %cmp to i32, !dbg !127
  %2 = call i32 @recordBasicBlock(i32 28, i32 8)
  ret i32 %conv, !dbg !127
}

; Function Attrs: nounwind
define i32 @Own_Above_Threat() #0 {
entry:
  %0 = load i32* @Other_Tracked_Alt, align 4, !dbg !128
  %1 = load i32* @Own_Tracked_Alt, align 4, !dbg !128
  %cmp = icmp slt i32 %0, %1, !dbg !128
  %conv = zext i1 %cmp to i32, !dbg !128
  %2 = call i32 @recordBasicBlock(i32 29, i32 8)
  ret i32 %conv, !dbg !128
}

; Function Attrs: nounwind
define i32 @alt_sep_test() #0 {
entry:
  %enabled = alloca i32, align 4
  %tcas_equipped = alloca i32, align 4
  %intent_not_known = alloca i32, align 4
  %need_upward_RA = alloca i32, align 4
  %need_downward_RA = alloca i32, align 4
  %alt_sep = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %enabled}, metadata !129), !dbg !130
  call void @llvm.dbg.declare(metadata !{i32* %tcas_equipped}, metadata !131), !dbg !132
  call void @llvm.dbg.declare(metadata !{i32* %intent_not_known}, metadata !133), !dbg !134
  call void @llvm.dbg.declare(metadata !{i32* %need_upward_RA}, metadata !135), !dbg !136
  call void @llvm.dbg.declare(metadata !{i32* %need_downward_RA}, metadata !137), !dbg !138
  call void @llvm.dbg.declare(metadata !{i32* %alt_sep}, metadata !139), !dbg !140
  %0 = load i32* @High_Confidence, align 4, !dbg !141
  %tobool = icmp ne i32 %0, 0, !dbg !141
  %1 = call i32 @recordBasicBlock(i32 30, i32 8)
  br i1 %tobool, label %land.lhs.true, label %land.end, !dbg !141

land.lhs.true:                                    ; preds = %entry
  %2 = load i32* @Own_Tracked_Alt_Rate, align 4, !dbg !142
  %cmp = icmp sle i32 %2, 600, !dbg !142
  %3 = call i32 @recordBasicBlock(i32 31, i32 8)
  br i1 %cmp, label %land.rhs, label %land.end, !dbg !142

land.rhs:                                         ; preds = %land.lhs.true
  %4 = load i32* @Cur_Vertical_Sep, align 4, !dbg !144
  %cmp1 = icmp sgt i32 %4, 600, !dbg !144
  %5 = call i32 @recordBasicBlock(i32 32, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %6 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %6 to i32, !dbg !146
  store i32 %land.ext, i32* %enabled, align 4, !dbg !146
  %7 = load i32* @Other_Capability, align 4, !dbg !149
  %cmp2 = icmp eq i32 %7, 1, !dbg !149
  %conv = zext i1 %cmp2 to i32, !dbg !149
  store i32 %conv, i32* %tcas_equipped, align 4, !dbg !149
  %8 = load i32* @Two_of_Three_Reports_Valid, align 4, !dbg !150
  %tobool3 = icmp ne i32 %8, 0, !dbg !150
  %9 = call i32 @recordBasicBlock(i32 33, i32 8)
  br i1 %tobool3, label %land.rhs4, label %land.end7, !dbg !150

land.rhs4:                                        ; preds = %land.end
  %10 = load i32* @Other_RAC, align 4, !dbg !151
  %cmp5 = icmp eq i32 %10, 0, !dbg !151
  %11 = call i32 @recordBasicBlock(i32 34, i32 8)
  br label %land.end7

land.end7:                                        ; preds = %land.rhs4, %land.end
  %12 = phi i1 [ false, %land.end ], [ %cmp5, %land.rhs4 ]
  %land.ext8 = zext i1 %12 to i32, !dbg !153
  store i32 %land.ext8, i32* %intent_not_known, align 4, !dbg !153
  store i32 0, i32* %alt_sep, align 4, !dbg !155
  %13 = load i32* %enabled, align 4, !dbg !156
  %tobool9 = icmp ne i32 %13, 0, !dbg !156
  %14 = call i32 @recordBasicBlock(i32 35, i32 8)
  br i1 %tobool9, label %land.lhs.true10, label %if.end40, !dbg !156

land.lhs.true10:                                  ; preds = %land.end7
  %15 = load i32* %tcas_equipped, align 4, !dbg !158
  %tobool11 = icmp ne i32 %15, 0, !dbg !158
  %16 = call i32 @recordBasicBlock(i32 36, i32 8)
  br i1 %tobool11, label %land.lhs.true12, label %lor.lhs.false, !dbg !158

land.lhs.true12:                                  ; preds = %land.lhs.true10
  %17 = load i32* %intent_not_known, align 4, !dbg !160
  %tobool13 = icmp ne i32 %17, 0, !dbg !160
  %18 = call i32 @recordBasicBlock(i32 37, i32 8)
  br i1 %tobool13, label %if.then, label %lor.lhs.false, !dbg !160

lor.lhs.false:                                    ; preds = %land.lhs.true12, %land.lhs.true10
  %19 = load i32* %tcas_equipped, align 4, !dbg !162
  %tobool14 = icmp ne i32 %19, 0, !dbg !162
  %20 = call i32 @recordBasicBlock(i32 38, i32 8)
  br i1 %tobool14, label %if.end40, label %if.then, !dbg !162

if.then:                                          ; preds = %lor.lhs.false, %land.lhs.true12
  %call = call i32 @Non_Crossing_Biased_Climb(), !dbg !165
  %tobool15 = icmp ne i32 %call, 0, !dbg !165
  %21 = call i32 @recordBasicBlock(i32 39, i32 8)
  br i1 %tobool15, label %land.rhs16, label %land.end19, !dbg !165

land.rhs16:                                       ; preds = %if.then
  %call17 = call i32 @Own_Below_Threat(), !dbg !167
  %tobool18 = icmp ne i32 %call17, 0, !dbg !167
  %22 = call i32 @recordBasicBlock(i32 40, i32 8)
  br label %land.end19

land.end19:                                       ; preds = %land.rhs16, %if.then
  %23 = phi i1 [ false, %if.then ], [ %tobool18, %land.rhs16 ]
  %land.ext20 = zext i1 %23 to i32, !dbg !169
  store i32 %land.ext20, i32* %need_upward_RA, align 4, !dbg !169
  %call21 = call i32 @Non_Crossing_Biased_Descend(), !dbg !171
  %tobool22 = icmp ne i32 %call21, 0, !dbg !171
  %24 = call i32 @recordBasicBlock(i32 41, i32 8)
  br i1 %tobool22, label %land.rhs23, label %land.end26, !dbg !171

land.rhs23:                                       ; preds = %land.end19
  %call24 = call i32 @Own_Above_Threat(), !dbg !172
  %tobool25 = icmp ne i32 %call24, 0, !dbg !172
  %25 = call i32 @recordBasicBlock(i32 42, i32 8)
  br label %land.end26

land.end26:                                       ; preds = %land.rhs23, %land.end19
  %26 = phi i1 [ false, %land.end19 ], [ %tobool25, %land.rhs23 ]
  %land.ext27 = zext i1 %26 to i32, !dbg !174
  store i32 %land.ext27, i32* %need_downward_RA, align 4, !dbg !174
  %27 = load i32* %need_upward_RA, align 4, !dbg !176
  %tobool28 = icmp ne i32 %27, 0, !dbg !176
  %28 = call i32 @recordBasicBlock(i32 43, i32 8)
  br i1 %tobool28, label %land.lhs.true29, label %if.else, !dbg !176

land.lhs.true29:                                  ; preds = %land.end26
  %29 = load i32* %need_downward_RA, align 4, !dbg !178
  %tobool30 = icmp ne i32 %29, 0, !dbg !178
  %30 = call i32 @recordBasicBlock(i32 44, i32 8)
  br i1 %tobool30, label %if.then31, label %if.else, !dbg !178

if.then31:                                        ; preds = %land.lhs.true29
  store i32 0, i32* %alt_sep, align 4, !dbg !180
  %31 = call i32 @recordBasicBlock(i32 45, i32 8)
  br label %if.end39, !dbg !180

if.else:                                          ; preds = %land.lhs.true29, %land.end26
  %32 = load i32* %need_upward_RA, align 4, !dbg !181
  %tobool32 = icmp ne i32 %32, 0, !dbg !181
  %33 = call i32 @recordBasicBlock(i32 46, i32 8)
  br i1 %tobool32, label %if.then33, label %if.else34, !dbg !181

if.then33:                                        ; preds = %if.else
  store i32 1, i32* %alt_sep, align 4, !dbg !183
  %34 = call i32 @recordBasicBlock(i32 47, i32 8)
  br label %if.end38, !dbg !183

if.else34:                                        ; preds = %if.else
  %35 = load i32* %need_downward_RA, align 4, !dbg !184
  %tobool35 = icmp ne i32 %35, 0, !dbg !184
  %36 = call i32 @recordBasicBlock(i32 48, i32 8)
  br i1 %tobool35, label %if.then36, label %if.else37, !dbg !184

if.then36:                                        ; preds = %if.else34
  store i32 2, i32* %alt_sep, align 4, !dbg !186
  %37 = call i32 @recordBasicBlock(i32 49, i32 8)
  br label %if.end, !dbg !186

if.else37:                                        ; preds = %if.else34
  store i32 0, i32* %alt_sep, align 4, !dbg !187
  %38 = call i32 @recordBasicBlock(i32 50, i32 8)
  br label %if.end

if.end:                                           ; preds = %if.else37, %if.then36
  %39 = call i32 @recordBasicBlock(i32 51, i32 8)
  br label %if.end38

if.end38:                                         ; preds = %if.end, %if.then33
  %40 = call i32 @recordBasicBlock(i32 52, i32 8)
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.then31
  %41 = call i32 @recordBasicBlock(i32 53, i32 8)
  br label %if.end40, !dbg !188

if.end40:                                         ; preds = %if.end39, %lor.lhs.false, %land.end7
  %42 = load i32* %alt_sep, align 4, !dbg !189
  %43 = call i32 @recordBasicBlock(i32 54, i32 8)
  ret i32 %42, !dbg !189
}

; Function Attrs: nounwind
define void @main(i32 %argc, i8** %argv) #0 {
entry:
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  store i32 %argc, i32* %argc.addr, align 4
  call void @llvm.dbg.declare(metadata !{i32* %argc.addr}, metadata !190), !dbg !191
  store i8** %argv, i8*** %argv.addr, align 4
  call void @llvm.dbg.declare(metadata !{i8*** %argv.addr}, metadata !192), !dbg !193
  %0 = load i32* %argc.addr, align 4, !dbg !194
  %cmp = icmp slt i32 %0, 13, !dbg !194
  %1 = call i32 @recordBasicBlock(i32 55, i32 8)
  %2 = call i32 @atexit(void ()* @savePath)
  br i1 %cmp, label %if.then, label %if.end, !dbg !194

if.then:                                          ; preds = %entry
  %3 = load %struct._IO_FILE** @stdout, align 4, !dbg !196
  %call = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([35 x i8]* @.str, i32 0, i32 0)), !dbg !196
  %4 = load %struct._IO_FILE** @stdout, align 4, !dbg !198
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([63 x i8]* @.str1, i32 0, i32 0)), !dbg !198
  %5 = load %struct._IO_FILE** @stdout, align 4, !dbg !199
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([58 x i8]* @.str2, i32 0, i32 0)), !dbg !199
  %6 = load %struct._IO_FILE** @stdout, align 4, !dbg !200
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([49 x i8]* @.str3, i32 0, i32 0)), !dbg !200
  %7 = load %struct._IO_FILE** @stdout, align 4, !dbg !201
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([44 x i8]* @.str4, i32 0, i32 0)), !dbg !201
  call void @exit(i32 1) #4, !dbg !202
  %8 = call i32 @recordBasicBlock(i32 56, i32 8)
  unreachable, !dbg !202

if.end:                                           ; preds = %entry
  call void @initialize(), !dbg !203
  %9 = load i8*** %argv.addr, align 4, !dbg !204
  %arrayidx = getelementptr inbounds i8** %9, i32 1, !dbg !204
  %10 = load i8** %arrayidx, align 4, !dbg !204
  %call5 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %10), !dbg !204
  store i32 %call5, i32* @Cur_Vertical_Sep, align 4, !dbg !204
  %11 = load i8*** %argv.addr, align 4, !dbg !205
  %arrayidx6 = getelementptr inbounds i8** %11, i32 2, !dbg !205
  %12 = load i8** %arrayidx6, align 4, !dbg !205
  %call7 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %12), !dbg !205
  store i32 %call7, i32* @High_Confidence, align 4, !dbg !205
  %13 = load i8*** %argv.addr, align 4, !dbg !206
  %arrayidx8 = getelementptr inbounds i8** %13, i32 3, !dbg !206
  %14 = load i8** %arrayidx8, align 4, !dbg !206
  %call9 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %14), !dbg !206
  store i32 %call9, i32* @Two_of_Three_Reports_Valid, align 4, !dbg !206
  %15 = load i8*** %argv.addr, align 4, !dbg !207
  %arrayidx10 = getelementptr inbounds i8** %15, i32 4, !dbg !207
  %16 = load i8** %arrayidx10, align 4, !dbg !207
  %call11 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %16), !dbg !207
  store i32 %call11, i32* @Own_Tracked_Alt, align 4, !dbg !207
  %17 = load i8*** %argv.addr, align 4, !dbg !208
  %arrayidx12 = getelementptr inbounds i8** %17, i32 5, !dbg !208
  %18 = load i8** %arrayidx12, align 4, !dbg !208
  %call13 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %18), !dbg !208
  store i32 %call13, i32* @Own_Tracked_Alt_Rate, align 4, !dbg !208
  %19 = load i8*** %argv.addr, align 4, !dbg !209
  %arrayidx14 = getelementptr inbounds i8** %19, i32 6, !dbg !209
  %20 = load i8** %arrayidx14, align 4, !dbg !209
  %call15 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %20), !dbg !209
  store i32 %call15, i32* @Other_Tracked_Alt, align 4, !dbg !209
  %21 = load i8*** %argv.addr, align 4, !dbg !210
  %arrayidx16 = getelementptr inbounds i8** %21, i32 7, !dbg !210
  %22 = load i8** %arrayidx16, align 4, !dbg !210
  %call17 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %22), !dbg !210
  store i32 %call17, i32* @Alt_Layer_Value, align 4, !dbg !210
  %23 = load i8*** %argv.addr, align 4, !dbg !211
  %arrayidx18 = getelementptr inbounds i8** %23, i32 8, !dbg !211
  %24 = load i8** %arrayidx18, align 4, !dbg !211
  %call19 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %24), !dbg !211
  store i32 %call19, i32* @Up_Separation, align 4, !dbg !211
  %25 = load i8*** %argv.addr, align 4, !dbg !212
  %arrayidx20 = getelementptr inbounds i8** %25, i32 9, !dbg !212
  %26 = load i8** %arrayidx20, align 4, !dbg !212
  %call21 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %26), !dbg !212
  store i32 %call21, i32* @Down_Separation, align 4, !dbg !212
  %27 = load i8*** %argv.addr, align 4, !dbg !213
  %arrayidx22 = getelementptr inbounds i8** %27, i32 10, !dbg !213
  %28 = load i8** %arrayidx22, align 4, !dbg !213
  %call23 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %28), !dbg !213
  store i32 %call23, i32* @Other_RAC, align 4, !dbg !213
  %29 = load i8*** %argv.addr, align 4, !dbg !214
  %arrayidx24 = getelementptr inbounds i8** %29, i32 11, !dbg !214
  %30 = load i8** %arrayidx24, align 4, !dbg !214
  %call25 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %30), !dbg !214
  store i32 %call25, i32* @Other_Capability, align 4, !dbg !214
  %31 = load i8*** %argv.addr, align 4, !dbg !215
  %arrayidx26 = getelementptr inbounds i8** %31, i32 12, !dbg !215
  %32 = load i8** %arrayidx26, align 4, !dbg !215
  %call27 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %32), !dbg !215
  store i32 %call27, i32* @Climb_Inhibit, align 4, !dbg !215
  %33 = load %struct._IO_FILE** @stdout, align 4, !dbg !216
  %call28 = call i32 @alt_sep_test(), !dbg !217
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([4 x i8]* @.str5, i32 0, i32 0), i32 %call28), !dbg !216
  call void @exit(i32 0) #4, !dbg !218
  %34 = call i32 @recordBasicBlock(i32 57, i32 8)
  unreachable, !dbg !218

return:                                           ; No predecessors!
  %35 = call i32 @recordBasicBlock(i32 58, i32 8)
  ret void, !dbg !219
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: noreturn
declare void @exit(i32) #3

declare i32 @atoi(...) #2

declare i32 @recordBasicBlock(i32, i32)

declare void @savePath()

declare i32 @atexit(void ()*)

attributes #0 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { noreturn }

!llvm.dbg.cu = !{!0}
!llvm.module.flags = !{!44, !45}
!llvm.ident = !{!46}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"clang version 3.5.0 (tags/RELEASE_350/final)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !27, metadata !2, metadata !"", i32 1} ; [ DW_TAG_compile_unit ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c] [DW_LANG_C99]
!1 = metadata !{metadata !"tcas.c", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source"}
!2 = metadata !{}
!3 = metadata !{metadata !4, metadata !8, metadata !12, metadata !13, metadata !17, metadata !18, metadata !19, metadata !20, metadata !21}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"initialize", metadata !"initialize", metadata !"", i32 48, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @initialize, null, null, metadata !2, i32 49} ; [ DW_TAG_subprogram ] [line 48] [def] [scope 49] [initialize]
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{null}
!8 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"ALIM", metadata !"ALIM", metadata !"", i32 56, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @ALIM, null, null, metadata !2, i32 57} ; [ DW_TAG_subprogram ] [line 56] [def] [scope 57] [ALIM]
!9 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !10, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!10 = metadata !{metadata !11}
!11 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!12 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Inhibit_Biased_Climb", metadata !"Inhibit_Biased_Climb", metadata !"", i32 61, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Inhibit_Biased_Climb, null, null, metadata !2, i32 62} ; [ DW_TAG_subprogram ] [line 61] [def] [scope 62] [Inhibit_Biased_Climb]
!13 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Non_Crossing_Biased_Climb", metadata !"Non_Crossing_Biased_Climb", metadata !"", i32 66, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Non_Crossing_Biased_Climb, null, null, metadata !2, i32 67} ; [ DW_TAG_subprogram ] [line 66] [def] [scope 67] [Non_Crossing_Biased_Climb]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{metadata !16}
!16 = metadata !{i32 786454, metadata !1, null, metadata !"bool", i32 16, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ] [bool] [line 16, size 0, align 0, offset 0] [from int]
!17 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Non_Crossing_Biased_Descend", metadata !"Non_Crossing_Biased_Descend", metadata !"", i32 84, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Non_Crossing_Biased_Descend, null, null, metadata !2, i32 85} ; [ DW_TAG_subprogram ] [line 84] [def] [scope 85] [Non_Crossing_Biased_Descend]
!18 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Own_Below_Threat", metadata !"Own_Below_Threat", metadata !"", i32 102, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Own_Below_Threat, null, null, metadata !2, i32 103} ; [ DW_TAG_subprogram ] [line 102] [def] [scope 103] [Own_Below_Threat]
!19 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Own_Above_Threat", metadata !"Own_Above_Threat", metadata !"", i32 107, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Own_Above_Threat, null, null, metadata !2, i32 108} ; [ DW_TAG_subprogram ] [line 107] [def] [scope 108] [Own_Above_Threat]
!20 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"alt_sep_test", metadata !"alt_sep_test", metadata !"", i32 112, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @alt_sep_test, null, null, metadata !2, i32 113} ; [ DW_TAG_subprogram ] [line 112] [def] [scope 113] [alt_sep_test]
!21 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 144, metadata !22, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i8**)* @main, null, null, metadata !2, i32 145} ; [ DW_TAG_subprogram ] [line 144] [def] [scope 145] [main]
!22 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !23, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!23 = metadata !{null, metadata !11, metadata !24}
!24 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 0, metadata !25} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [from ]
!25 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 0, metadata !26} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [from char]
!26 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!27 = metadata !{metadata !28, metadata !29, metadata !30, metadata !31, metadata !32, metadata !33, metadata !34, metadata !35, metadata !39, metadata !40, metadata !41, metadata !42, metadata !43}
!28 = metadata !{i32 786484, i32 0, null, metadata !"Cur_Vertical_Sep", metadata !"Cur_Vertical_Sep", metadata !"", metadata !5, i32 18, metadata !11, i32 0, i32 1, i32* @Cur_Vertical_Sep, null} ; [ DW_TAG_variable ] [Cur_Vertical_Sep] [line 18] [def]
!29 = metadata !{i32 786484, i32 0, null, metadata !"High_Confidence", metadata !"High_Confidence", metadata !"", metadata !5, i32 19, metadata !16, i32 0, i32 1, i32* @High_Confidence, null} ; [ DW_TAG_variable ] [High_Confidence] [line 19] [def]
!30 = metadata !{i32 786484, i32 0, null, metadata !"Two_of_Three_Reports_Valid", metadata !"Two_of_Three_Reports_Valid", metadata !"", metadata !5, i32 20, metadata !16, i32 0, i32 1, i32* @Two_of_Three_Reports_Valid, null} ; [ DW_TAG_variable ] [Two_of_Three_Reports_Valid] [line 20] [def]
!31 = metadata !{i32 786484, i32 0, null, metadata !"Own_Tracked_Alt", metadata !"Own_Tracked_Alt", metadata !"", metadata !5, i32 22, metadata !11, i32 0, i32 1, i32* @Own_Tracked_Alt, null} ; [ DW_TAG_variable ] [Own_Tracked_Alt] [line 22] [def]
!32 = metadata !{i32 786484, i32 0, null, metadata !"Own_Tracked_Alt_Rate", metadata !"Own_Tracked_Alt_Rate", metadata !"", metadata !5, i32 23, metadata !11, i32 0, i32 1, i32* @Own_Tracked_Alt_Rate, null} ; [ DW_TAG_variable ] [Own_Tracked_Alt_Rate] [line 23] [def]
!33 = metadata !{i32 786484, i32 0, null, metadata !"Other_Tracked_Alt", metadata !"Other_Tracked_Alt", metadata !"", metadata !5, i32 24, metadata !11, i32 0, i32 1, i32* @Other_Tracked_Alt, null} ; [ DW_TAG_variable ] [Other_Tracked_Alt] [line 24] [def]
!34 = metadata !{i32 786484, i32 0, null, metadata !"Alt_Layer_Value", metadata !"Alt_Layer_Value", metadata !"", metadata !5, i32 26, metadata !11, i32 0, i32 1, i32* @Alt_Layer_Value, null} ; [ DW_TAG_variable ] [Alt_Layer_Value] [line 26] [def]
!35 = metadata !{i32 786484, i32 0, null, metadata !"Positive_RA_Alt_Thresh", metadata !"Positive_RA_Alt_Thresh", metadata !"", metadata !5, i32 27, metadata !36, i32 0, i32 1, [4 x i32]* @Positive_RA_Alt_Thresh, null} ; [ DW_TAG_variable ] [Positive_RA_Alt_Thresh] [line 27] [def]
!36 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 128, i64 32, i32 0, i32 0, metadata !11, metadata !37, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 128, align 32, offset 0] [from int]
!37 = metadata !{metadata !38}
!38 = metadata !{i32 786465, i64 0, i64 4}        ; [ DW_TAG_subrange_type ] [0, 3]
!39 = metadata !{i32 786484, i32 0, null, metadata !"Up_Separation", metadata !"Up_Separation", metadata !"", metadata !5, i32 29, metadata !11, i32 0, i32 1, i32* @Up_Separation, null} ; [ DW_TAG_variable ] [Up_Separation] [line 29] [def]
!40 = metadata !{i32 786484, i32 0, null, metadata !"Down_Separation", metadata !"Down_Separation", metadata !"", metadata !5, i32 30, metadata !11, i32 0, i32 1, i32* @Down_Separation, null} ; [ DW_TAG_variable ] [Down_Separation] [line 30] [def]
!41 = metadata !{i32 786484, i32 0, null, metadata !"Other_RAC", metadata !"Other_RAC", metadata !"", metadata !5, i32 33, metadata !11, i32 0, i32 1, i32* @Other_RAC, null} ; [ DW_TAG_variable ] [Other_RAC] [line 33] [def]
!42 = metadata !{i32 786484, i32 0, null, metadata !"Other_Capability", metadata !"Other_Capability", metadata !"", metadata !5, i32 38, metadata !11, i32 0, i32 1, i32* @Other_Capability, null} ; [ DW_TAG_variable ] [Other_Capability] [line 38] [def]
!43 = metadata !{i32 786484, i32 0, null, metadata !"Climb_Inhibit", metadata !"Climb_Inhibit", metadata !"", metadata !5, i32 42, metadata !11, i32 0, i32 1, i32* @Climb_Inhibit, null} ; [ DW_TAG_variable ] [Climb_Inhibit] [line 42] [def]
!44 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!45 = metadata !{i32 2, metadata !"Debug Info Version", i32 1}
!46 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
!47 = metadata !{i32 50, i32 5, metadata !4, null}
!48 = metadata !{i32 51, i32 5, metadata !4, null}
!49 = metadata !{i32 52, i32 5, metadata !4, null}
!50 = metadata !{i32 53, i32 5, metadata !4, null}
!51 = metadata !{i32 54, i32 1, metadata !4, null}
!52 = metadata !{i32 58, i32 2, metadata !8, null} ; [ DW_TAG_imported_module ]
!53 = metadata !{i32 63, i32 5, metadata !12, null}
!54 = metadata !{i32 63, i32 5, metadata !55, null}
!55 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 1, i32 13} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!56 = metadata !{i32 63, i32 5, metadata !57, null}
!57 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 2, i32 14} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!58 = metadata !{i32 63, i32 5, metadata !59, null}
!59 = metadata !{i32 786443, metadata !1, metadata !60, i32 63, i32 5, i32 4, i32 16} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!60 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 3, i32 15} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!61 = metadata !{i32 786688, metadata !13, metadata !"upward_preferred", metadata !5, i32 68, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_preferred] [line 68]
!62 = metadata !{i32 68, i32 9, metadata !13, null}
!63 = metadata !{i32 786688, metadata !13, metadata !"upward_crossing_situation", metadata !5, i32 69, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_crossing_situation] [line 69]
!64 = metadata !{i32 69, i32 9, metadata !13, null}
!65 = metadata !{i32 786688, metadata !13, metadata !"result", metadata !5, i32 70, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [result] [line 70]
!66 = metadata !{i32 70, i32 10, metadata !13, null}
!67 = metadata !{i32 72, i32 24, metadata !13, null}
!68 = metadata !{i32 73, i32 9, metadata !69, null}
!69 = metadata !{i32 786443, metadata !1, metadata !13, i32 73, i32 9, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!70 = metadata !{i32 75, i32 13, metadata !71, null}
!71 = metadata !{i32 786443, metadata !1, metadata !69, i32 74, i32 5, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!72 = metadata !{i32 75, i32 38, metadata !73, null}
!73 = metadata !{i32 786443, metadata !1, metadata !71, i32 75, i32 38, i32 1, i32 17} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!74 = metadata !{i32 75, i32 38, metadata !75, null}
!75 = metadata !{i32 786443, metadata !1, metadata !71, i32 75, i32 38, i32 3, i32 19} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!76 = metadata !{i32 75, i32 83, metadata !71, null}
!77 = metadata !{i32 75, i32 83, metadata !78, null}
!78 = metadata !{i32 786443, metadata !1, metadata !71, i32 75, i32 83, i32 4, i32 20} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!79 = metadata !{i32 75, i32 83, metadata !80, null}
!80 = metadata !{i32 786443, metadata !1, metadata !81, i32 75, i32 83, i32 5, i32 21} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!81 = metadata !{i32 786443, metadata !1, metadata !71, i32 75, i32 83, i32 2, i32 18} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!82 = metadata !{i32 76, i32 5, metadata !71, null}
!83 = metadata !{i32 79, i32 11, metadata !84, null}
!84 = metadata !{i32 786443, metadata !1, metadata !69, i32 78, i32 5, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!85 = metadata !{i32 79, i32 11, metadata !86, null}
!86 = metadata !{i32 786443, metadata !1, metadata !84, i32 79, i32 11, i32 1, i32 22} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!87 = metadata !{i32 79, i32 11, metadata !88, null}
!88 = metadata !{i32 786443, metadata !1, metadata !84, i32 79, i32 11, i32 3, i32 24} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!89 = metadata !{i32 79, i32 83, metadata !84, null}
!90 = metadata !{i32 79, i32 83, metadata !91, null}
!91 = metadata !{i32 786443, metadata !1, metadata !92, i32 79, i32 83, i32 4, i32 25} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!92 = metadata !{i32 786443, metadata !1, metadata !84, i32 79, i32 83, i32 2, i32 23} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!93 = metadata !{i32 81, i32 5, metadata !13, null}
!94 = metadata !{i32 786688, metadata !17, metadata !"upward_preferred", metadata !5, i32 86, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_preferred] [line 86]
!95 = metadata !{i32 86, i32 9, metadata !17, null}
!96 = metadata !{i32 786688, metadata !17, metadata !"upward_crossing_situation", metadata !5, i32 87, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_crossing_situation] [line 87]
!97 = metadata !{i32 87, i32 9, metadata !17, null}
!98 = metadata !{i32 786688, metadata !17, metadata !"result", metadata !5, i32 88, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [result] [line 88]
!99 = metadata !{i32 88, i32 10, metadata !17, null}
!100 = metadata !{i32 90, i32 24, metadata !17, null}
!101 = metadata !{i32 91, i32 9, metadata !102, null}
!102 = metadata !{i32 786443, metadata !1, metadata !17, i32 91, i32 9, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!103 = metadata !{i32 93, i32 11, metadata !104, null}
!104 = metadata !{i32 786443, metadata !1, metadata !102, i32 92, i32 5, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!105 = metadata !{i32 93, i32 11, metadata !106, null}
!106 = metadata !{i32 786443, metadata !1, metadata !104, i32 93, i32 11, i32 1, i32 26} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!107 = metadata !{i32 93, i32 11, metadata !108, null}
!108 = metadata !{i32 786443, metadata !1, metadata !104, i32 93, i32 11, i32 3, i32 28} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!109 = metadata !{i32 93, i32 85, metadata !104, null}
!110 = metadata !{i32 93, i32 85, metadata !111, null}
!111 = metadata !{i32 786443, metadata !1, metadata !112, i32 93, i32 85, i32 4, i32 29} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!112 = metadata !{i32 786443, metadata !1, metadata !104, i32 93, i32 85, i32 2, i32 27} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!113 = metadata !{i32 94, i32 5, metadata !104, null}
!114 = metadata !{i32 97, i32 13, metadata !115, null}
!115 = metadata !{i32 786443, metadata !1, metadata !102, i32 96, i32 5, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!116 = metadata !{i32 97, i32 38, metadata !117, null}
!117 = metadata !{i32 786443, metadata !1, metadata !115, i32 97, i32 38, i32 1, i32 30} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!118 = metadata !{i32 97, i32 38, metadata !119, null}
!119 = metadata !{i32 786443, metadata !1, metadata !115, i32 97, i32 38, i32 3, i32 32} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!120 = metadata !{i32 97, i32 79, metadata !115, null}
!121 = metadata !{i32 97, i32 79, metadata !122, null}
!122 = metadata !{i32 786443, metadata !1, metadata !115, i32 97, i32 79, i32 4, i32 33} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!123 = metadata !{i32 97, i32 79, metadata !124, null}
!124 = metadata !{i32 786443, metadata !1, metadata !125, i32 97, i32 79, i32 5, i32 34} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!125 = metadata !{i32 786443, metadata !1, metadata !115, i32 97, i32 79, i32 2, i32 31} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!126 = metadata !{i32 99, i32 5, metadata !17, null}
!127 = metadata !{i32 104, i32 5, metadata !18, null}
!128 = metadata !{i32 109, i32 5, metadata !19, null}
!129 = metadata !{i32 786688, metadata !20, metadata !"enabled", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [enabled] [line 114]
!130 = metadata !{i32 114, i32 10, metadata !20, null}
!131 = metadata !{i32 786688, metadata !20, metadata !"tcas_equipped", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [tcas_equipped] [line 114]
!132 = metadata !{i32 114, i32 19, metadata !20, null}
!133 = metadata !{i32 786688, metadata !20, metadata !"intent_not_known", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [intent_not_known] [line 114]
!134 = metadata !{i32 114, i32 34, metadata !20, null}
!135 = metadata !{i32 786688, metadata !20, metadata !"need_upward_RA", metadata !5, i32 115, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [need_upward_RA] [line 115]
!136 = metadata !{i32 115, i32 10, metadata !20, null}
!137 = metadata !{i32 786688, metadata !20, metadata !"need_downward_RA", metadata !5, i32 115, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [need_downward_RA] [line 115]
!138 = metadata !{i32 115, i32 26, metadata !20, null}
!139 = metadata !{i32 786688, metadata !20, metadata !"alt_sep", metadata !5, i32 116, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [alt_sep] [line 116]
!140 = metadata !{i32 116, i32 9, metadata !20, null}
!141 = metadata !{i32 118, i32 5, metadata !20, null}
!142 = metadata !{i32 118, i32 5, metadata !143, null}
!143 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 1, i32 35} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!144 = metadata !{i32 118, i32 5, metadata !145, null}
!145 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 3, i32 37} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!146 = metadata !{i32 118, i32 5, metadata !147, null}
!147 = metadata !{i32 786443, metadata !1, metadata !148, i32 118, i32 5, i32 4, i32 38} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!148 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 2, i32 36} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!149 = metadata !{i32 119, i32 5, metadata !20, null}
!150 = metadata !{i32 120, i32 5, metadata !20, null}
!151 = metadata !{i32 120, i32 5, metadata !152, null}
!152 = metadata !{i32 786443, metadata !1, metadata !20, i32 120, i32 5, i32 1, i32 39} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!153 = metadata !{i32 120, i32 5, metadata !154, null}
!154 = metadata !{i32 786443, metadata !1, metadata !20, i32 120, i32 5, i32 2, i32 40} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!155 = metadata !{i32 122, i32 5, metadata !20, null}
!156 = metadata !{i32 124, i32 9, metadata !157, null}
!157 = metadata !{i32 786443, metadata !1, metadata !20, i32 124, i32 9, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!158 = metadata !{i32 124, i32 9, metadata !159, null}
!159 = metadata !{i32 786443, metadata !1, metadata !157, i32 124, i32 9, i32 1, i32 41} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!160 = metadata !{i32 124, i32 9, metadata !161, null}
!161 = metadata !{i32 786443, metadata !1, metadata !157, i32 124, i32 9, i32 2, i32 42} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!162 = metadata !{i32 124, i32 9, metadata !163, null}
!163 = metadata !{i32 786443, metadata !1, metadata !164, i32 124, i32 9, i32 4, i32 44} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!164 = metadata !{i32 786443, metadata !1, metadata !157, i32 124, i32 9, i32 3, i32 43} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!165 = metadata !{i32 126, i32 19, metadata !166, null}
!166 = metadata !{i32 786443, metadata !1, metadata !157, i32 125, i32 5, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!167 = metadata !{i32 126, i32 50, metadata !168, null}
!168 = metadata !{i32 786443, metadata !1, metadata !166, i32 126, i32 50, i32 1, i32 45} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!169 = metadata !{i32 126, i32 50, metadata !170, null}
!170 = metadata !{i32 786443, metadata !1, metadata !166, i32 126, i32 50, i32 2, i32 46} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!171 = metadata !{i32 127, i32 21, metadata !166, null}
!172 = metadata !{i32 127, i32 54, metadata !173, null}
!173 = metadata !{i32 786443, metadata !1, metadata !166, i32 127, i32 54, i32 1, i32 47} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!174 = metadata !{i32 127, i32 54, metadata !175, null}
!175 = metadata !{i32 786443, metadata !1, metadata !166, i32 127, i32 54, i32 2, i32 48} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!176 = metadata !{i32 128, i32 6, metadata !177, null}
!177 = metadata !{i32 786443, metadata !1, metadata !166, i32 128, i32 6, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!178 = metadata !{i32 128, i32 6, metadata !179, null}
!179 = metadata !{i32 786443, metadata !1, metadata !177, i32 128, i32 6, i32 1, i32 49} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!180 = metadata !{i32 132, i32 6, metadata !177, null}
!181 = metadata !{i32 133, i32 11, metadata !182, null}
!182 = metadata !{i32 786443, metadata !1, metadata !177, i32 133, i32 11, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!183 = metadata !{i32 134, i32 6, metadata !182, null}
!184 = metadata !{i32 135, i32 11, metadata !185, null}
!185 = metadata !{i32 786443, metadata !1, metadata !182, i32 135, i32 11, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!186 = metadata !{i32 136, i32 6, metadata !185, null}
!187 = metadata !{i32 138, i32 6, metadata !185, null}
!188 = metadata !{i32 139, i32 5, metadata !166, null}
!189 = metadata !{i32 141, i32 5, metadata !20, null}
!190 = metadata !{i32 786689, metadata !21, metadata !"argc", metadata !5, i32 16777360, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [argc] [line 144]
!191 = metadata !{i32 144, i32 15, metadata !21, null}
!192 = metadata !{i32 786689, metadata !21, metadata !"argv", metadata !5, i32 33554576, metadata !24, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [argv] [line 144]
!193 = metadata !{i32 144, i32 27, metadata !21, null}
!194 = metadata !{i32 146, i32 8, metadata !195, null}
!195 = metadata !{i32 786443, metadata !1, metadata !21, i32 146, i32 8, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!196 = metadata !{i32 148, i32 2, metadata !197, null}
!197 = metadata !{i32 786443, metadata !1, metadata !195, i32 147, i32 5, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!198 = metadata !{i32 149, i32 2, metadata !197, null}
!199 = metadata !{i32 150, i32 2, metadata !197, null}
!200 = metadata !{i32 151, i32 2, metadata !197, null}
!201 = metadata !{i32 152, i32 2, metadata !197, null}
!202 = metadata !{i32 153, i32 2, metadata !197, null}
!203 = metadata !{i32 155, i32 5, metadata !21, null}
!204 = metadata !{i32 156, i32 24, metadata !21, null}
!205 = metadata !{i32 157, i32 23, metadata !21, null}
!206 = metadata !{i32 158, i32 34, metadata !21, null}
!207 = metadata !{i32 159, i32 23, metadata !21, null}
!208 = metadata !{i32 160, i32 28, metadata !21, null}
!209 = metadata !{i32 161, i32 25, metadata !21, null}
!210 = metadata !{i32 162, i32 23, metadata !21, null}
!211 = metadata !{i32 163, i32 21, metadata !21, null}
!212 = metadata !{i32 164, i32 23, metadata !21, null}
!213 = metadata !{i32 165, i32 17, metadata !21, null}
!214 = metadata !{i32 166, i32 24, metadata !21, null}
!215 = metadata !{i32 167, i32 21, metadata !21, null}
!216 = metadata !{i32 169, i32 5, metadata !21, null}
!217 = metadata !{i32 169, i32 29, metadata !21, null}
!218 = metadata !{i32 170, i32 5, metadata !21, null}
!219 = metadata !{i32 171, i32 1, metadata !21, null}
