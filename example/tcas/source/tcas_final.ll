; ModuleID = 'tcas_final.bc'
target datalayout = "e-m:e-p:32:32-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i386-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i32, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i32, i32, [40 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }
%"class.std::ios_base::Init" = type { i8 }
%"class.std::basic_ofstream" = type { %"class.std::basic_ostream.base", %"class.std::basic_filebuf", %"class.std::basic_ios" }
%"class.std::basic_ostream.base" = type { i32 (...)** }
%"class.std::basic_filebuf" = type { %"class.std::basic_streambuf", %union.pthread_mutex_t, %"class.std::__basic_file", i32, %struct.__mbstate_t, %struct.__mbstate_t, %struct.__mbstate_t, i8*, i32, i8, i8, i8, i8, i8*, i8*, i8, %"class.std::codecvt"*, i8*, i32, i8*, i8* }
%"class.std::basic_streambuf" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"class.std::locale" }
%"class.std::locale" = type { %"class.std::locale::_Impl"* }
%"class.std::locale::_Impl" = type { i32, %"class.std::locale::facet"**, i32, %"class.std::locale::facet"**, i8** }
%"class.std::locale::facet" = type { i32 (...)**, i32 }
%union.pthread_mutex_t = type { %"struct.(anonymous union)::__pthread_mutex_s" }
%"struct.(anonymous union)::__pthread_mutex_s" = type { i32, i32, i32, i32, i32, %union.anon }
%union.anon = type { i32 }
%"class.std::__basic_file" = type { %struct._IO_FILE*, i8 }
%struct.__mbstate_t = type { i32, %union.anon.0 }
%union.anon.0 = type { i32 }
%"class.std::codecvt" = type { %"class.std::__codecvt_abstract_base", %struct.__locale_struct* }
%"class.std::__codecvt_abstract_base" = type { %"class.std::locale::facet" }
%struct.__locale_struct = type { [13 x %struct.__locale_data*], i16*, i32*, i32*, [13 x i8*] }
%struct.__locale_data = type opaque
%"class.std::basic_ios" = type { %"class.std::ios_base", %"class.std::basic_ostream"*, i8, i8, %"class.std::basic_streambuf"*, %"class.std::ctype"*, %"class.std::num_put"*, %"class.std::num_get"* }
%"class.std::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"class.std::locale" }
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"class.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i32 }
%"class.std::basic_ostream" = type { i32 (...)**, %"class.std::basic_ios" }
%"class.std::ctype" = type { %"class.std::locale::facet", %struct.__locale_struct*, i8, i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8 }
%"class.std::num_put" = type { %"class.std::locale::facet" }
%"class.std::num_get" = type { %"class.std::locale::facet" }

@Positive_RA_Alt_Thresh = common global [4 x i32] zeroinitializer, align 4
@Alt_Layer_Value = common global i32 0, align 4
@Climb_Inhibit = common global i32 0, align 4
@Up_Separation = common global i32 0, align 4
@Down_Separation = common global i32 0, align 4
@Cur_Vertical_Sep = common global i32 0, align 4
@Own_Tracked_Alt = common global i32 0, align 4
@Other_Tracked_Alt = common global i32 0, align 4
@High_Confidence = common global i32 0, align 4
@Own_Tracked_Alt_Rate = common global i32 0, align 4
@Other_Capability = common global i32 0, align 4
@Two_of_Three_Reports_Valid = common global i32 0, align 4
@Other_RAC = common global i32 0, align 4
@stdout = external global %struct._IO_FILE*
@.str = private unnamed_addr constant [35 x i8] c"Error: Command line arguments are\0A\00", align 1
@.str1 = private unnamed_addr constant [63 x i8] c"Cur_Vertical_Sep, High_Confidence, Two_of_Three_Reports_Valid\0A\00", align 1
@.str2 = private unnamed_addr constant [58 x i8] c"Own_Tracked_Alt, Own_Tracked_Alt_Rate, Other_Tracked_Alt\0A\00", align 1
@.str3 = private unnamed_addr constant [49 x i8] c"Alt_Layer_Value, Up_Separation, Down_Separation\0A\00", align 1
@.str4 = private unnamed_addr constant [44 x i8] c"Other_RAC, Other_Capability, Climb_Inhibit\0A\00", align 1
@.str5 = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1
@_ZStL8__ioinit = internal global %"class.std::ios_base::Init" zeroinitializer, align 1
@__dso_handle = external global i8
@out = global %"class.std::basic_ofstream" zeroinitializer, align 4
@.str6 = private unnamed_addr constant [14 x i8] c"exec_path.txt\00", align 1
@path = global [1000000 x i32] zeroinitializer, align 4
@_subpathLength = global i32 0, align 4
@count = global i32 0, align 4
@_ZZ16recordBasicBlockE10firstBlock = internal global i8 1, align 1
@_ZZ16recordBasicBlockE8n_MAXNUM = internal global i32 0, align 4
@_ZSt4cerr = external global %"class.std::basic_ostream"
@.str27 = private unnamed_addr constant [16 x i8] c"path length is \00", align 1
@.str38 = private unnamed_addr constant [10 x i8] c"printPath\00", align 1
@.str49 = private unnamed_addr constant [2 x i8] c" \00", align 1
@.str510 = private unnamed_addr constant [2 x i8] c"(\00", align 1
@.str611 = private unnamed_addr constant [8 x i8] c")count=\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_ExecutePathForSmallCase.cpp, i8* null }]

; Function Attrs: nounwind
define void @initialize() #0 {
entry:
  store i32 400, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 0), align 4, !dbg !186
  store i32 500, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 1), align 4, !dbg !187
  store i32 640, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 2), align 4, !dbg !188
  store i32 740, i32* getelementptr inbounds ([4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 3), align 4, !dbg !189
  %0 = call i32 @recordBasicBlock(i32 0, i32 8)
  ret void, !dbg !190
}

; Function Attrs: nounwind
define i32 @ALIM() #0 {
entry:
  %0 = load i32* @Alt_Layer_Value, align 4, !dbg !191
  %arrayidx = getelementptr inbounds [4 x i32]* @Positive_RA_Alt_Thresh, i32 0, i32 %0, !dbg !191
  %1 = load i32* %arrayidx, align 4, !dbg !191
  %2 = call i32 @recordBasicBlock(i32 1, i32 8)
  ret i32 %1, !dbg !191
}

; Function Attrs: nounwind
define i32 @Inhibit_Biased_Climb() #0 {
entry:
  %0 = load i32* @Climb_Inhibit, align 4, !dbg !192
  %tobool = icmp ne i32 %0, 0, !dbg !192
  %1 = call i32 @recordBasicBlock(i32 2, i32 8)
  br i1 %tobool, label %cond.true, label %cond.false, !dbg !192

cond.true:                                        ; preds = %entry
  %2 = load i32* @Up_Separation, align 4, !dbg !193
  %add = add nsw i32 %2, 100, !dbg !193
  %3 = call i32 @recordBasicBlock(i32 3, i32 8)
  br label %cond.end, !dbg !193

cond.false:                                       ; preds = %entry
  %4 = load i32* @Up_Separation, align 4, !dbg !195
  %5 = call i32 @recordBasicBlock(i32 4, i32 8)
  br label %cond.end, !dbg !195

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ %4, %cond.false ], !dbg !192
  %6 = call i32 @recordBasicBlock(i32 5, i32 8)
  ret i32 %cond, !dbg !197
}

; Function Attrs: nounwind
define i32 @Non_Crossing_Biased_Climb() #0 {
entry:
  %upward_preferred = alloca i32, align 4
  %upward_crossing_situation = alloca i32, align 4
  %result = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %upward_preferred}, metadata !200), !dbg !201
  call void @llvm.dbg.declare(metadata !{i32* %upward_crossing_situation}, metadata !202), !dbg !203
  call void @llvm.dbg.declare(metadata !{i32* %result}, metadata !204), !dbg !205
  %call = call i32 @Inhibit_Biased_Climb(), !dbg !206
  %0 = load i32* @Down_Separation, align 4, !dbg !206
  %cmp = icmp sgt i32 %call, %0, !dbg !206
  %conv = zext i1 %cmp to i32, !dbg !206
  store i32 %conv, i32* %upward_preferred, align 4, !dbg !206
  %1 = load i32* %upward_preferred, align 4, !dbg !207
  %tobool = icmp ne i32 %1, 0, !dbg !207
  %2 = call i32 @recordBasicBlock(i32 6, i32 8)
  br i1 %tobool, label %if.then, label %if.else, !dbg !207

if.then:                                          ; preds = %entry
  %call1 = call i32 @Own_Below_Threat(), !dbg !209
  %tobool2 = icmp ne i32 %call1, 0, !dbg !209
  %3 = call i32 @recordBasicBlock(i32 7, i32 8)
  br i1 %tobool2, label %lor.rhs, label %lor.end, !dbg !209

lor.rhs:                                          ; preds = %if.then
  %call3 = call i32 @Own_Below_Threat(), !dbg !211
  %tobool4 = icmp ne i32 %call3, 0, !dbg !211
  %4 = call i32 @recordBasicBlock(i32 8, i32 8)
  br i1 %tobool4, label %land.rhs, label %land.end, !dbg !211

land.rhs:                                         ; preds = %lor.rhs
  %5 = load i32* @Down_Separation, align 4, !dbg !213
  %call5 = call i32 @ALIM(), !dbg !215
  %cmp6 = icmp sge i32 %5, %call5, !dbg !215
  %lnot = xor i1 %cmp6, true, !dbg !215
  %6 = call i32 @recordBasicBlock(i32 9, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %lor.rhs
  %7 = phi i1 [ false, %lor.rhs ], [ %lnot, %land.rhs ]
  %8 = call i32 @recordBasicBlock(i32 10, i32 8)
  br label %lor.end, !dbg !216

lor.end:                                          ; preds = %land.end, %if.then
  %9 = phi i1 [ true, %if.then ], [ %7, %land.end ]
  %lor.ext = zext i1 %9 to i32, !dbg !218
  store i32 %lor.ext, i32* %result, align 4, !dbg !218
  %10 = call i32 @recordBasicBlock(i32 11, i32 8)
  br label %if.end, !dbg !221

if.else:                                          ; preds = %entry
  %call8 = call i32 @Own_Above_Threat(), !dbg !222
  %tobool9 = icmp ne i32 %call8, 0, !dbg !222
  %11 = call i32 @recordBasicBlock(i32 12, i32 8)
  br i1 %tobool9, label %land.lhs.true, label %land.end16, !dbg !222

land.lhs.true:                                    ; preds = %if.else
  %12 = load i32* @Cur_Vertical_Sep, align 4, !dbg !224
  %cmp10 = icmp sge i32 %12, 300, !dbg !224
  %13 = call i32 @recordBasicBlock(i32 13, i32 8)
  br i1 %cmp10, label %land.rhs12, label %land.end16, !dbg !224

land.rhs12:                                       ; preds = %land.lhs.true
  %14 = load i32* @Up_Separation, align 4, !dbg !226
  %call13 = call i32 @ALIM(), !dbg !228
  %cmp14 = icmp sge i32 %14, %call13, !dbg !228
  %15 = call i32 @recordBasicBlock(i32 14, i32 8)
  br label %land.end16

land.end16:                                       ; preds = %land.rhs12, %land.lhs.true, %if.else
  %16 = phi i1 [ false, %land.lhs.true ], [ false, %if.else ], [ %cmp14, %land.rhs12 ]
  %land.ext = zext i1 %16 to i32, !dbg !229
  store i32 %land.ext, i32* %result, align 4, !dbg !229
  %17 = call i32 @recordBasicBlock(i32 15, i32 8)
  br label %if.end

if.end:                                           ; preds = %land.end16, %lor.end
  %18 = load i32* %result, align 4, !dbg !232
  %19 = call i32 @recordBasicBlock(i32 16, i32 8)
  ret i32 %18, !dbg !232
}

; Function Attrs: nounwind readnone
declare void @llvm.dbg.declare(metadata, metadata) #1

; Function Attrs: nounwind
define i32 @Non_Crossing_Biased_Descend() #0 {
entry:
  %upward_preferred = alloca i32, align 4
  %upward_crossing_situation = alloca i32, align 4
  %result = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %upward_preferred}, metadata !233), !dbg !234
  call void @llvm.dbg.declare(metadata !{i32* %upward_crossing_situation}, metadata !235), !dbg !236
  call void @llvm.dbg.declare(metadata !{i32* %result}, metadata !237), !dbg !238
  %call = call i32 @Inhibit_Biased_Climb(), !dbg !239
  %0 = load i32* @Down_Separation, align 4, !dbg !239
  %cmp = icmp sgt i32 %call, %0, !dbg !239
  %conv = zext i1 %cmp to i32, !dbg !239
  store i32 %conv, i32* %upward_preferred, align 4, !dbg !239
  %1 = load i32* %upward_preferred, align 4, !dbg !240
  %tobool = icmp ne i32 %1, 0, !dbg !240
  %2 = call i32 @recordBasicBlock(i32 17, i32 8)
  br i1 %tobool, label %if.then, label %if.else, !dbg !240

if.then:                                          ; preds = %entry
  %call1 = call i32 @Own_Below_Threat(), !dbg !242
  %tobool2 = icmp ne i32 %call1, 0, !dbg !242
  %3 = call i32 @recordBasicBlock(i32 18, i32 8)
  br i1 %tobool2, label %land.lhs.true, label %land.end, !dbg !242

land.lhs.true:                                    ; preds = %if.then
  %4 = load i32* @Cur_Vertical_Sep, align 4, !dbg !244
  %cmp3 = icmp sge i32 %4, 300, !dbg !244
  %5 = call i32 @recordBasicBlock(i32 19, i32 8)
  br i1 %cmp3, label %land.rhs, label %land.end, !dbg !244

land.rhs:                                         ; preds = %land.lhs.true
  %6 = load i32* @Down_Separation, align 4, !dbg !246
  %call5 = call i32 @ALIM(), !dbg !248
  %cmp6 = icmp sge i32 %6, %call5, !dbg !248
  %7 = call i32 @recordBasicBlock(i32 20, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %if.then
  %8 = phi i1 [ false, %land.lhs.true ], [ false, %if.then ], [ %cmp6, %land.rhs ]
  %land.ext = zext i1 %8 to i32, !dbg !249
  store i32 %land.ext, i32* %result, align 4, !dbg !249
  %9 = call i32 @recordBasicBlock(i32 21, i32 8)
  br label %if.end, !dbg !252

if.else:                                          ; preds = %entry
  %call8 = call i32 @Own_Above_Threat(), !dbg !253
  %tobool9 = icmp ne i32 %call8, 0, !dbg !253
  %10 = call i32 @recordBasicBlock(i32 22, i32 8)
  br i1 %tobool9, label %lor.rhs, label %lor.end, !dbg !253

lor.rhs:                                          ; preds = %if.else
  %call10 = call i32 @Own_Above_Threat(), !dbg !255
  %tobool11 = icmp ne i32 %call10, 0, !dbg !255
  %11 = call i32 @recordBasicBlock(i32 23, i32 8)
  br i1 %tobool11, label %land.rhs12, label %land.end16, !dbg !255

land.rhs12:                                       ; preds = %lor.rhs
  %12 = load i32* @Up_Separation, align 4, !dbg !257
  %call13 = call i32 @ALIM(), !dbg !259
  %cmp14 = icmp sge i32 %12, %call13, !dbg !259
  %13 = call i32 @recordBasicBlock(i32 24, i32 8)
  br label %land.end16

land.end16:                                       ; preds = %land.rhs12, %lor.rhs
  %14 = phi i1 [ false, %lor.rhs ], [ %cmp14, %land.rhs12 ]
  %15 = call i32 @recordBasicBlock(i32 25, i32 8)
  br label %lor.end, !dbg !260

lor.end:                                          ; preds = %land.end16, %if.else
  %16 = phi i1 [ true, %if.else ], [ %14, %land.end16 ]
  %lor.ext = zext i1 %16 to i32, !dbg !262
  store i32 %lor.ext, i32* %result, align 4, !dbg !262
  %17 = call i32 @recordBasicBlock(i32 26, i32 8)
  br label %if.end

if.end:                                           ; preds = %lor.end, %land.end
  %18 = load i32* %result, align 4, !dbg !265
  %19 = call i32 @recordBasicBlock(i32 27, i32 8)
  ret i32 %18, !dbg !265
}

; Function Attrs: nounwind
define i32 @Own_Below_Threat() #0 {
entry:
  %0 = load i32* @Own_Tracked_Alt, align 4, !dbg !266
  %1 = load i32* @Other_Tracked_Alt, align 4, !dbg !266
  %cmp = icmp slt i32 %0, %1, !dbg !266
  %conv = zext i1 %cmp to i32, !dbg !266
  %2 = call i32 @recordBasicBlock(i32 28, i32 8)
  ret i32 %conv, !dbg !266
}

; Function Attrs: nounwind
define i32 @Own_Above_Threat() #0 {
entry:
  %0 = load i32* @Other_Tracked_Alt, align 4, !dbg !267
  %1 = load i32* @Own_Tracked_Alt, align 4, !dbg !267
  %cmp = icmp slt i32 %0, %1, !dbg !267
  %conv = zext i1 %cmp to i32, !dbg !267
  %2 = call i32 @recordBasicBlock(i32 29, i32 8)
  ret i32 %conv, !dbg !267
}

; Function Attrs: nounwind
define i32 @alt_sep_test() #0 {
entry:
  %enabled = alloca i32, align 4
  %tcas_equipped = alloca i32, align 4
  %intent_not_known = alloca i32, align 4
  %need_upward_RA = alloca i32, align 4
  %need_downward_RA = alloca i32, align 4
  %alt_sep = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %enabled}, metadata !268), !dbg !269
  call void @llvm.dbg.declare(metadata !{i32* %tcas_equipped}, metadata !270), !dbg !271
  call void @llvm.dbg.declare(metadata !{i32* %intent_not_known}, metadata !272), !dbg !273
  call void @llvm.dbg.declare(metadata !{i32* %need_upward_RA}, metadata !274), !dbg !275
  call void @llvm.dbg.declare(metadata !{i32* %need_downward_RA}, metadata !276), !dbg !277
  call void @llvm.dbg.declare(metadata !{i32* %alt_sep}, metadata !278), !dbg !279
  %0 = load i32* @High_Confidence, align 4, !dbg !280
  %tobool = icmp ne i32 %0, 0, !dbg !280
  %1 = call i32 @recordBasicBlock(i32 30, i32 8)
  br i1 %tobool, label %land.lhs.true, label %land.end, !dbg !280

land.lhs.true:                                    ; preds = %entry
  %2 = load i32* @Own_Tracked_Alt_Rate, align 4, !dbg !281
  %cmp = icmp sle i32 %2, 600, !dbg !281
  %3 = call i32 @recordBasicBlock(i32 31, i32 8)
  br i1 %cmp, label %land.rhs, label %land.end, !dbg !281

land.rhs:                                         ; preds = %land.lhs.true
  %4 = load i32* @Cur_Vertical_Sep, align 4, !dbg !283
  %cmp1 = icmp sgt i32 %4, 600, !dbg !283
  %5 = call i32 @recordBasicBlock(i32 32, i32 8)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %entry
  %6 = phi i1 [ false, %land.lhs.true ], [ false, %entry ], [ %cmp1, %land.rhs ]
  %land.ext = zext i1 %6 to i32, !dbg !285
  store i32 %land.ext, i32* %enabled, align 4, !dbg !285
  %7 = load i32* @Other_Capability, align 4, !dbg !288
  %cmp2 = icmp eq i32 %7, 1, !dbg !288
  %conv = zext i1 %cmp2 to i32, !dbg !288
  store i32 %conv, i32* %tcas_equipped, align 4, !dbg !288
  %8 = load i32* @Two_of_Three_Reports_Valid, align 4, !dbg !289
  %tobool3 = icmp ne i32 %8, 0, !dbg !289
  %9 = call i32 @recordBasicBlock(i32 33, i32 8)
  br i1 %tobool3, label %land.rhs4, label %land.end7, !dbg !289

land.rhs4:                                        ; preds = %land.end
  %10 = load i32* @Other_RAC, align 4, !dbg !290
  %cmp5 = icmp eq i32 %10, 0, !dbg !290
  %11 = call i32 @recordBasicBlock(i32 34, i32 8)
  br label %land.end7

land.end7:                                        ; preds = %land.rhs4, %land.end
  %12 = phi i1 [ false, %land.end ], [ %cmp5, %land.rhs4 ]
  %land.ext8 = zext i1 %12 to i32, !dbg !292
  store i32 %land.ext8, i32* %intent_not_known, align 4, !dbg !292
  store i32 0, i32* %alt_sep, align 4, !dbg !294
  %13 = load i32* %enabled, align 4, !dbg !295
  %tobool9 = icmp ne i32 %13, 0, !dbg !295
  %14 = call i32 @recordBasicBlock(i32 35, i32 8)
  br i1 %tobool9, label %land.lhs.true10, label %if.end40, !dbg !295

land.lhs.true10:                                  ; preds = %land.end7
  %15 = load i32* %tcas_equipped, align 4, !dbg !297
  %tobool11 = icmp ne i32 %15, 0, !dbg !297
  %16 = call i32 @recordBasicBlock(i32 36, i32 8)
  br i1 %tobool11, label %land.lhs.true12, label %lor.lhs.false, !dbg !297

land.lhs.true12:                                  ; preds = %land.lhs.true10
  %17 = load i32* %intent_not_known, align 4, !dbg !299
  %tobool13 = icmp ne i32 %17, 0, !dbg !299
  %18 = call i32 @recordBasicBlock(i32 37, i32 8)
  br i1 %tobool13, label %if.then, label %lor.lhs.false, !dbg !299

lor.lhs.false:                                    ; preds = %land.lhs.true12, %land.lhs.true10
  %19 = load i32* %tcas_equipped, align 4, !dbg !301
  %tobool14 = icmp ne i32 %19, 0, !dbg !301
  %20 = call i32 @recordBasicBlock(i32 38, i32 8)
  br i1 %tobool14, label %if.end40, label %if.then, !dbg !301

if.then:                                          ; preds = %lor.lhs.false, %land.lhs.true12
  %call = call i32 @Non_Crossing_Biased_Climb(), !dbg !304
  %tobool15 = icmp ne i32 %call, 0, !dbg !304
  %21 = call i32 @recordBasicBlock(i32 39, i32 8)
  br i1 %tobool15, label %land.rhs16, label %land.end19, !dbg !304

land.rhs16:                                       ; preds = %if.then
  %call17 = call i32 @Own_Below_Threat(), !dbg !306
  %tobool18 = icmp ne i32 %call17, 0, !dbg !306
  %22 = call i32 @recordBasicBlock(i32 40, i32 8)
  br label %land.end19

land.end19:                                       ; preds = %land.rhs16, %if.then
  %23 = phi i1 [ false, %if.then ], [ %tobool18, %land.rhs16 ]
  %land.ext20 = zext i1 %23 to i32, !dbg !308
  store i32 %land.ext20, i32* %need_upward_RA, align 4, !dbg !308
  %call21 = call i32 @Non_Crossing_Biased_Descend(), !dbg !310
  %tobool22 = icmp ne i32 %call21, 0, !dbg !310
  %24 = call i32 @recordBasicBlock(i32 41, i32 8)
  br i1 %tobool22, label %land.rhs23, label %land.end26, !dbg !310

land.rhs23:                                       ; preds = %land.end19
  %call24 = call i32 @Own_Above_Threat(), !dbg !311
  %tobool25 = icmp ne i32 %call24, 0, !dbg !311
  %25 = call i32 @recordBasicBlock(i32 42, i32 8)
  br label %land.end26

land.end26:                                       ; preds = %land.rhs23, %land.end19
  %26 = phi i1 [ false, %land.end19 ], [ %tobool25, %land.rhs23 ]
  %land.ext27 = zext i1 %26 to i32, !dbg !313
  store i32 %land.ext27, i32* %need_downward_RA, align 4, !dbg !313
  %27 = load i32* %need_upward_RA, align 4, !dbg !315
  %tobool28 = icmp ne i32 %27, 0, !dbg !315
  %28 = call i32 @recordBasicBlock(i32 43, i32 8)
  br i1 %tobool28, label %land.lhs.true29, label %if.else, !dbg !315

land.lhs.true29:                                  ; preds = %land.end26
  %29 = load i32* %need_downward_RA, align 4, !dbg !317
  %tobool30 = icmp ne i32 %29, 0, !dbg !317
  %30 = call i32 @recordBasicBlock(i32 44, i32 8)
  br i1 %tobool30, label %if.then31, label %if.else, !dbg !317

if.then31:                                        ; preds = %land.lhs.true29
  store i32 0, i32* %alt_sep, align 4, !dbg !319
  %31 = call i32 @recordBasicBlock(i32 45, i32 8)
  br label %if.end39, !dbg !319

if.else:                                          ; preds = %land.lhs.true29, %land.end26
  %32 = load i32* %need_upward_RA, align 4, !dbg !320
  %tobool32 = icmp ne i32 %32, 0, !dbg !320
  %33 = call i32 @recordBasicBlock(i32 46, i32 8)
  br i1 %tobool32, label %if.then33, label %if.else34, !dbg !320

if.then33:                                        ; preds = %if.else
  store i32 1, i32* %alt_sep, align 4, !dbg !322
  %34 = call i32 @recordBasicBlock(i32 47, i32 8)
  br label %if.end38, !dbg !322

if.else34:                                        ; preds = %if.else
  %35 = load i32* %need_downward_RA, align 4, !dbg !323
  %tobool35 = icmp ne i32 %35, 0, !dbg !323
  %36 = call i32 @recordBasicBlock(i32 48, i32 8)
  br i1 %tobool35, label %if.then36, label %if.else37, !dbg !323

if.then36:                                        ; preds = %if.else34
  store i32 2, i32* %alt_sep, align 4, !dbg !325
  %37 = call i32 @recordBasicBlock(i32 49, i32 8)
  br label %if.end, !dbg !325

if.else37:                                        ; preds = %if.else34
  store i32 0, i32* %alt_sep, align 4, !dbg !326
  %38 = call i32 @recordBasicBlock(i32 50, i32 8)
  br label %if.end

if.end:                                           ; preds = %if.else37, %if.then36
  %39 = call i32 @recordBasicBlock(i32 51, i32 8)
  br label %if.end38

if.end38:                                         ; preds = %if.end, %if.then33
  %40 = call i32 @recordBasicBlock(i32 52, i32 8)
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.then31
  %41 = call i32 @recordBasicBlock(i32 53, i32 8)
  br label %if.end40, !dbg !327

if.end40:                                         ; preds = %if.end39, %lor.lhs.false, %land.end7
  %42 = load i32* %alt_sep, align 4, !dbg !328
  %43 = call i32 @recordBasicBlock(i32 54, i32 8)
  ret i32 %42, !dbg !328
}

; Function Attrs: nounwind
define void @main(i32 %argc, i8** %argv) #0 {
entry:
  %argc.addr = alloca i32, align 4
  %argv.addr = alloca i8**, align 4
  store i32 %argc, i32* %argc.addr, align 4
  call void @llvm.dbg.declare(metadata !{i32* %argc.addr}, metadata !329), !dbg !330
  store i8** %argv, i8*** %argv.addr, align 4
  call void @llvm.dbg.declare(metadata !{i8*** %argv.addr}, metadata !331), !dbg !332
  %0 = load i32* %argc.addr, align 4, !dbg !333
  %cmp = icmp slt i32 %0, 13, !dbg !333
  %1 = call i32 @recordBasicBlock(i32 55, i32 8)
  %2 = call i32 @atexit(void ()* @savePath)
  br i1 %cmp, label %if.then, label %if.end, !dbg !333

if.then:                                          ; preds = %entry
  %3 = load %struct._IO_FILE** @stdout, align 4, !dbg !335
  %call = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %3, i8* getelementptr inbounds ([35 x i8]* @.str, i32 0, i32 0)), !dbg !335
  %4 = load %struct._IO_FILE** @stdout, align 4, !dbg !337
  %call1 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %4, i8* getelementptr inbounds ([63 x i8]* @.str1, i32 0, i32 0)), !dbg !337
  %5 = load %struct._IO_FILE** @stdout, align 4, !dbg !338
  %call2 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %5, i8* getelementptr inbounds ([58 x i8]* @.str2, i32 0, i32 0)), !dbg !338
  %6 = load %struct._IO_FILE** @stdout, align 4, !dbg !339
  %call3 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %6, i8* getelementptr inbounds ([49 x i8]* @.str3, i32 0, i32 0)), !dbg !339
  %7 = load %struct._IO_FILE** @stdout, align 4, !dbg !340
  %call4 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %7, i8* getelementptr inbounds ([44 x i8]* @.str4, i32 0, i32 0)), !dbg !340
  call void @exit(i32 1) #5, !dbg !341
  %8 = call i32 @recordBasicBlock(i32 56, i32 8)
  unreachable, !dbg !341

if.end:                                           ; preds = %entry
  call void @initialize(), !dbg !342
  %9 = load i8*** %argv.addr, align 4, !dbg !343
  %arrayidx = getelementptr inbounds i8** %9, i32 1, !dbg !343
  %10 = load i8** %arrayidx, align 4, !dbg !343
  %call5 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %10), !dbg !343
  store i32 %call5, i32* @Cur_Vertical_Sep, align 4, !dbg !343
  %11 = load i8*** %argv.addr, align 4, !dbg !344
  %arrayidx6 = getelementptr inbounds i8** %11, i32 2, !dbg !344
  %12 = load i8** %arrayidx6, align 4, !dbg !344
  %call7 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %12), !dbg !344
  store i32 %call7, i32* @High_Confidence, align 4, !dbg !344
  %13 = load i8*** %argv.addr, align 4, !dbg !345
  %arrayidx8 = getelementptr inbounds i8** %13, i32 3, !dbg !345
  %14 = load i8** %arrayidx8, align 4, !dbg !345
  %call9 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %14), !dbg !345
  store i32 %call9, i32* @Two_of_Three_Reports_Valid, align 4, !dbg !345
  %15 = load i8*** %argv.addr, align 4, !dbg !346
  %arrayidx10 = getelementptr inbounds i8** %15, i32 4, !dbg !346
  %16 = load i8** %arrayidx10, align 4, !dbg !346
  %call11 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %16), !dbg !346
  store i32 %call11, i32* @Own_Tracked_Alt, align 4, !dbg !346
  %17 = load i8*** %argv.addr, align 4, !dbg !347
  %arrayidx12 = getelementptr inbounds i8** %17, i32 5, !dbg !347
  %18 = load i8** %arrayidx12, align 4, !dbg !347
  %call13 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %18), !dbg !347
  store i32 %call13, i32* @Own_Tracked_Alt_Rate, align 4, !dbg !347
  %19 = load i8*** %argv.addr, align 4, !dbg !348
  %arrayidx14 = getelementptr inbounds i8** %19, i32 6, !dbg !348
  %20 = load i8** %arrayidx14, align 4, !dbg !348
  %call15 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %20), !dbg !348
  store i32 %call15, i32* @Other_Tracked_Alt, align 4, !dbg !348
  %21 = load i8*** %argv.addr, align 4, !dbg !349
  %arrayidx16 = getelementptr inbounds i8** %21, i32 7, !dbg !349
  %22 = load i8** %arrayidx16, align 4, !dbg !349
  %call17 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %22), !dbg !349
  store i32 %call17, i32* @Alt_Layer_Value, align 4, !dbg !349
  %23 = load i8*** %argv.addr, align 4, !dbg !350
  %arrayidx18 = getelementptr inbounds i8** %23, i32 8, !dbg !350
  %24 = load i8** %arrayidx18, align 4, !dbg !350
  %call19 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %24), !dbg !350
  store i32 %call19, i32* @Up_Separation, align 4, !dbg !350
  %25 = load i8*** %argv.addr, align 4, !dbg !351
  %arrayidx20 = getelementptr inbounds i8** %25, i32 9, !dbg !351
  %26 = load i8** %arrayidx20, align 4, !dbg !351
  %call21 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %26), !dbg !351
  store i32 %call21, i32* @Down_Separation, align 4, !dbg !351
  %27 = load i8*** %argv.addr, align 4, !dbg !352
  %arrayidx22 = getelementptr inbounds i8** %27, i32 10, !dbg !352
  %28 = load i8** %arrayidx22, align 4, !dbg !352
  %call23 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %28), !dbg !352
  store i32 %call23, i32* @Other_RAC, align 4, !dbg !352
  %29 = load i8*** %argv.addr, align 4, !dbg !353
  %arrayidx24 = getelementptr inbounds i8** %29, i32 11, !dbg !353
  %30 = load i8** %arrayidx24, align 4, !dbg !353
  %call25 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %30), !dbg !353
  store i32 %call25, i32* @Other_Capability, align 4, !dbg !353
  %31 = load i8*** %argv.addr, align 4, !dbg !354
  %arrayidx26 = getelementptr inbounds i8** %31, i32 12, !dbg !354
  %32 = load i8** %arrayidx26, align 4, !dbg !354
  %call27 = call i32 bitcast (i32 (...)* @atoi to i32 (i8*)*)(i8* %32), !dbg !354
  store i32 %call27, i32* @Climb_Inhibit, align 4, !dbg !354
  %33 = load %struct._IO_FILE** @stdout, align 4, !dbg !355
  %call28 = call i32 @alt_sep_test(), !dbg !356
  %call29 = call i32 (%struct._IO_FILE*, i8*, ...)* @fprintf(%struct._IO_FILE* %33, i8* getelementptr inbounds ([4 x i8]* @.str5, i32 0, i32 0), i32 %call28), !dbg !355
  call void @exit(i32 0) #5, !dbg !357
  %34 = call i32 @recordBasicBlock(i32 57, i32 8)
  unreachable, !dbg !357

return:                                           ; No predecessors!
  %35 = call i32 @recordBasicBlock(i32 58, i32 8)
  ret void, !dbg !358
}

declare i32 @fprintf(%struct._IO_FILE*, i8*, ...) #2

; Function Attrs: noreturn
declare void @exit(i32) #3

declare i32 @atoi(...) #2

declare i32 @atexit(void ()*)

declare void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"*) #2

declare void @_ZNSt8ios_base4InitD1Ev(%"class.std::ios_base::Init"*) #2

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #4

declare void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"*, i8*, i32) #2

declare void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev(%"class.std::basic_ofstream"*) #2

define i32 @recordBasicBlock(i32 %id, i32 %subpathLength) #2 {
entry:
  %id.addr = alloca i32, align 4
  %subpathLength.addr = alloca i32, align 4
  store i32 %id, i32* %id.addr, align 4
  call void @llvm.dbg.declare(metadata !{i32* %id.addr}, metadata !359), !dbg !360
  store i32 %subpathLength, i32* %subpathLength.addr, align 4
  call void @llvm.dbg.declare(metadata !{i32* %subpathLength.addr}, metadata !361), !dbg !362
  %0 = load i8* @_ZZ16recordBasicBlockE10firstBlock, align 1, !dbg !363
  %tobool = trunc i8 %0 to i1, !dbg !363
  br i1 %tobool, label %if.then, label %if.end, !dbg !363

if.then:                                          ; preds = %entry
  %1 = load i32* @count, align 4, !dbg !365
  %inc = add nsw i32 %1, 1, !dbg !365
  store i32 %inc, i32* @count, align 4, !dbg !365
  %arrayidx = getelementptr inbounds [1000000 x i32]* @path, i32 0, i32 %1, !dbg !365
  store i32 -1, i32* %arrayidx, align 4, !dbg !365
  %2 = load i32* %subpathLength.addr, align 4, !dbg !367
  store i32 %2, i32* @_subpathLength, align 4, !dbg !367
  store i8 0, i8* @_ZZ16recordBasicBlockE10firstBlock, align 1, !dbg !368
  br label %if.end, !dbg !369

if.end:                                           ; preds = %if.then, %entry
  %3 = load i32* %id.addr, align 4, !dbg !370
  %4 = load i32* @count, align 4, !dbg !370
  %inc1 = add nsw i32 %4, 1, !dbg !370
  store i32 %inc1, i32* @count, align 4, !dbg !370
  %arrayidx2 = getelementptr inbounds [1000000 x i32]* @path, i32 0, i32 %4, !dbg !370
  store i32 %3, i32* %arrayidx2, align 4, !dbg !370
  %5 = load i32* @count, align 4, !dbg !371
  %cmp = icmp eq i32 %5, 1000000, !dbg !371
  br i1 %cmp, label %if.then3, label %if.end7, !dbg !371

if.then3:                                         ; preds = %if.end
  call void @savePath(), !dbg !373
  store i32 0, i32* @count, align 4, !dbg !375
  %call = call dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140) @_ZSt4cerr, i8* getelementptr inbounds ([16 x i8]* @.str27, i32 0, i32 0)), !dbg !376
  %6 = load i32* @_ZZ16recordBasicBlockE8n_MAXNUM, align 4, !dbg !376
  %inc4 = add nsw i32 %6, 1, !dbg !376
  store i32 %inc4, i32* @_ZZ16recordBasicBlockE8n_MAXNUM, align 4, !dbg !376
  %mul = mul nsw i32 %inc4, 1000000, !dbg !376
  %call5 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call, i32 %mul), !dbg !376
  %call6 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call5, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), !dbg !376
  br label %if.end7, !dbg !377

if.end7:                                          ; preds = %if.then3, %if.end
  ret i32 0, !dbg !378
}

define void @savePath() #2 {
entry:
  %i = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !{i32* %i}, metadata !379), !dbg !381
  store i32 0, i32* %i, align 4, !dbg !382
  br label %for.cond, !dbg !382

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32* %i, align 4, !dbg !383
  %1 = load i32* @count, align 4, !dbg !383
  %cmp = icmp slt i32 %0, %1, !dbg !383
  br i1 %cmp, label %for.body, label %for.end, !dbg !383

for.body:                                         ; preds = %for.cond
  %2 = load i32* %i, align 4, !dbg !386
  %arrayidx = getelementptr inbounds [1000000 x i32]* @path, i32 0, i32 %2, !dbg !386
  %3 = load i32* %arrayidx, align 4, !dbg !386
  %call = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* bitcast (%"class.std::basic_ofstream"* @out to %"class.std::basic_ostream"*), i32 %3), !dbg !386
  %call1 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), !dbg !386
  br label %for.inc, !dbg !388

for.inc:                                          ; preds = %for.body
  %4 = load i32* %i, align 4, !dbg !389
  %inc = add nsw i32 %4, 1, !dbg !389
  store i32 %inc, i32* %i, align 4, !dbg !389
  br label %for.cond, !dbg !389

for.end:                                          ; preds = %for.cond
  ret void, !dbg !390
}

declare dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140), i8*) #2

declare dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"*, i32) #2

declare dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"*, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)*) #2

declare dereferenceable(140) %"class.std::basic_ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"class.std::basic_ostream"* dereferenceable(140)) #2

define void @printPath(i32 %currentInput) #2 {
entry:
  %currentInput.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %currentInput, i32* %currentInput.addr, align 4
  call void @llvm.dbg.declare(metadata !{i32* %currentInput.addr}, metadata !391), !dbg !392
  %call = call dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140) @_ZSt4cerr, i8* getelementptr inbounds ([10 x i8]* @.str38, i32 0, i32 0)), !dbg !393
  %call1 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), !dbg !393
  call void @llvm.dbg.declare(metadata !{i32* %i}, metadata !394), !dbg !396
  store i32 0, i32* %i, align 4, !dbg !397
  br label %for.cond, !dbg !397

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32* %i, align 4, !dbg !398
  %1 = load i32* @count, align 4, !dbg !398
  %cmp = icmp slt i32 %0, %1, !dbg !398
  br i1 %cmp, label %for.body, label %for.end, !dbg !398

for.body:                                         ; preds = %for.cond
  %2 = load i32* %i, align 4, !dbg !401
  %arrayidx = getelementptr inbounds [1000000 x i32]* @path, i32 0, i32 %2, !dbg !401
  %3 = load i32* %arrayidx, align 4, !dbg !401
  %call2 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* @_ZSt4cerr, i32 %3), !dbg !401
  %call3 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140) %call2, i8* getelementptr inbounds ([2 x i8]* @.str49, i32 0, i32 0)), !dbg !401
  br label %for.inc, !dbg !403

for.inc:                                          ; preds = %for.body
  %4 = load i32* %i, align 4, !dbg !404
  %inc = add nsw i32 %4, 1, !dbg !404
  store i32 %inc, i32* %i, align 4, !dbg !404
  br label %for.cond, !dbg !404

for.end:                                          ; preds = %for.cond
  %call4 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140) @_ZSt4cerr, i8* getelementptr inbounds ([2 x i8]* @.str510, i32 0, i32 0)), !dbg !405
  %5 = load i32* %currentInput.addr, align 4, !dbg !405
  %call5 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call4, i32 %5), !dbg !405
  %call6 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"class.std::basic_ostream"* dereferenceable(140) %call5, i8* getelementptr inbounds ([8 x i8]* @.str611, i32 0, i32 0)), !dbg !405
  %6 = load i32* @count, align 4, !dbg !405
  %call7 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEi(%"class.std::basic_ostream"* %call6, i32 %6), !dbg !405
  %call8 = call dereferenceable(140) %"class.std::basic_ostream"* @_ZNSolsEPFRSoS_E(%"class.std::basic_ostream"* %call7, %"class.std::basic_ostream"* (%"class.std::basic_ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_), !dbg !405
  ret void, !dbg !406
}

define internal void @__cxx_global_var_init() section ".text.startup" {
entry:
  call void @_ZNSt8ios_base4InitC1Ev(%"class.std::ios_base::Init"* @_ZStL8__ioinit), !dbg !407
  %0 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::ios_base::Init"*)* @_ZNSt8ios_base4InitD1Ev to void (i8*)*), i8* getelementptr inbounds (%"class.std::ios_base::Init"* @_ZStL8__ioinit, i32 0, i32 0), i8* @__dso_handle) #4, !dbg !407
  ret void, !dbg !407
}

define internal void @__cxx_global_var_init1() section ".text.startup" {
entry:
  call void @_ZNSt14basic_ofstreamIcSt11char_traitsIcEEC1EPKcSt13_Ios_Openmode(%"class.std::basic_ofstream"* @out, i8* getelementptr inbounds ([14 x i8]* @.str6, i32 0, i32 0), i32 1), !dbg !408
  %0 = call i32 @__cxa_atexit(void (i8*)* bitcast (void (%"class.std::basic_ofstream"*)* @_ZNSt14basic_ofstreamIcSt11char_traitsIcEED1Ev to void (i8*)*), i8* bitcast (%"class.std::basic_ofstream"* @out to i8*), i8* @__dso_handle) #4, !dbg !408
  ret void, !dbg !408
}

define internal void @_GLOBAL__sub_I_ExecutePathForSmallCase.cpp() section ".text.startup" {
entry:
  call void @__cxx_global_var_init(), !dbg !409
  call void @__cxx_global_var_init1(), !dbg !409
  ret void, !dbg !409
}

attributes #0 = { nounwind "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone }
attributes #2 = { "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { noreturn "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }
attributes #5 = { noreturn }

!llvm.dbg.cu = !{!0, !44}
!llvm.module.flags = !{!183, !184}
!llvm.ident = !{!185, !185}

!0 = metadata !{i32 786449, metadata !1, i32 12, metadata !"clang version 3.5.0 (tags/RELEASE_350/final)", i1 false, metadata !"", i32 0, metadata !2, metadata !2, metadata !3, metadata !27, metadata !2, metadata !"", i32 1} ; [ DW_TAG_compile_unit ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c] [DW_LANG_C99]
!1 = metadata !{metadata !"tcas.c", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source"}
!2 = metadata !{}
!3 = metadata !{metadata !4, metadata !8, metadata !12, metadata !13, metadata !17, metadata !18, metadata !19, metadata !20, metadata !21}
!4 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"initialize", metadata !"initialize", metadata !"", i32 48, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, void ()* @initialize, null, null, metadata !2, i32 49} ; [ DW_TAG_subprogram ] [line 48] [def] [scope 49] [initialize]
!5 = metadata !{i32 786473, metadata !1}          ; [ DW_TAG_file_type ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!6 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !7, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!7 = metadata !{null}
!8 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"ALIM", metadata !"ALIM", metadata !"", i32 56, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @ALIM, null, null, metadata !2, i32 57} ; [ DW_TAG_subprogram ] [line 56] [def] [scope 57] [ALIM]
!9 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !10, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!10 = metadata !{metadata !11}
!11 = metadata !{i32 786468, null, null, metadata !"int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!12 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Inhibit_Biased_Climb", metadata !"Inhibit_Biased_Climb", metadata !"", i32 61, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Inhibit_Biased_Climb, null, null, metadata !2, i32 62} ; [ DW_TAG_subprogram ] [line 61] [def] [scope 62] [Inhibit_Biased_Climb]
!13 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Non_Crossing_Biased_Climb", metadata !"Non_Crossing_Biased_Climb", metadata !"", i32 66, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Non_Crossing_Biased_Climb, null, null, metadata !2, i32 67} ; [ DW_TAG_subprogram ] [line 66] [def] [scope 67] [Non_Crossing_Biased_Climb]
!14 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !15, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!15 = metadata !{metadata !16}
!16 = metadata !{i32 786454, metadata !1, null, metadata !"bool", i32 16, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ] [bool] [line 16, size 0, align 0, offset 0] [from int]
!17 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Non_Crossing_Biased_Descend", metadata !"Non_Crossing_Biased_Descend", metadata !"", i32 84, metadata !14, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Non_Crossing_Biased_Descend, null, null, metadata !2, i32 85} ; [ DW_TAG_subprogram ] [line 84] [def] [scope 85] [Non_Crossing_Biased_Descend]
!18 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Own_Below_Threat", metadata !"Own_Below_Threat", metadata !"", i32 102, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Own_Below_Threat, null, null, metadata !2, i32 103} ; [ DW_TAG_subprogram ] [line 102] [def] [scope 103] [Own_Below_Threat]
!19 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"Own_Above_Threat", metadata !"Own_Above_Threat", metadata !"", i32 107, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @Own_Above_Threat, null, null, metadata !2, i32 108} ; [ DW_TAG_subprogram ] [line 107] [def] [scope 108] [Own_Above_Threat]
!20 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"alt_sep_test", metadata !"alt_sep_test", metadata !"", i32 112, metadata !9, i1 false, i1 true, i32 0, i32 0, null, i32 0, i1 false, i32 ()* @alt_sep_test, null, null, metadata !2, i32 113} ; [ DW_TAG_subprogram ] [line 112] [def] [scope 113] [alt_sep_test]
!21 = metadata !{i32 786478, metadata !1, metadata !5, metadata !"main", metadata !"main", metadata !"", i32 144, metadata !22, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32, i8**)* @main, null, null, metadata !2, i32 145} ; [ DW_TAG_subprogram ] [line 144] [def] [scope 145] [main]
!22 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !23, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!23 = metadata !{null, metadata !11, metadata !24}
!24 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 0, metadata !25} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [from ]
!25 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 0, metadata !26} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [from char]
!26 = metadata !{i32 786468, null, null, metadata !"char", i32 0, i64 8, i64 8, i64 0, i32 0, i32 6} ; [ DW_TAG_base_type ] [char] [line 0, size 8, align 8, offset 0, enc DW_ATE_signed_char]
!27 = metadata !{metadata !28, metadata !29, metadata !30, metadata !31, metadata !32, metadata !33, metadata !34, metadata !35, metadata !39, metadata !40, metadata !41, metadata !42, metadata !43}
!28 = metadata !{i32 786484, i32 0, null, metadata !"Cur_Vertical_Sep", metadata !"Cur_Vertical_Sep", metadata !"", metadata !5, i32 18, metadata !11, i32 0, i32 1, i32* @Cur_Vertical_Sep, null} ; [ DW_TAG_variable ] [Cur_Vertical_Sep] [line 18] [def]
!29 = metadata !{i32 786484, i32 0, null, metadata !"High_Confidence", metadata !"High_Confidence", metadata !"", metadata !5, i32 19, metadata !16, i32 0, i32 1, i32* @High_Confidence, null} ; [ DW_TAG_variable ] [High_Confidence] [line 19] [def]
!30 = metadata !{i32 786484, i32 0, null, metadata !"Two_of_Three_Reports_Valid", metadata !"Two_of_Three_Reports_Valid", metadata !"", metadata !5, i32 20, metadata !16, i32 0, i32 1, i32* @Two_of_Three_Reports_Valid, null} ; [ DW_TAG_variable ] [Two_of_Three_Reports_Valid] [line 20] [def]
!31 = metadata !{i32 786484, i32 0, null, metadata !"Own_Tracked_Alt", metadata !"Own_Tracked_Alt", metadata !"", metadata !5, i32 22, metadata !11, i32 0, i32 1, i32* @Own_Tracked_Alt, null} ; [ DW_TAG_variable ] [Own_Tracked_Alt] [line 22] [def]
!32 = metadata !{i32 786484, i32 0, null, metadata !"Own_Tracked_Alt_Rate", metadata !"Own_Tracked_Alt_Rate", metadata !"", metadata !5, i32 23, metadata !11, i32 0, i32 1, i32* @Own_Tracked_Alt_Rate, null} ; [ DW_TAG_variable ] [Own_Tracked_Alt_Rate] [line 23] [def]
!33 = metadata !{i32 786484, i32 0, null, metadata !"Other_Tracked_Alt", metadata !"Other_Tracked_Alt", metadata !"", metadata !5, i32 24, metadata !11, i32 0, i32 1, i32* @Other_Tracked_Alt, null} ; [ DW_TAG_variable ] [Other_Tracked_Alt] [line 24] [def]
!34 = metadata !{i32 786484, i32 0, null, metadata !"Alt_Layer_Value", metadata !"Alt_Layer_Value", metadata !"", metadata !5, i32 26, metadata !11, i32 0, i32 1, i32* @Alt_Layer_Value, null} ; [ DW_TAG_variable ] [Alt_Layer_Value] [line 26] [def]
!35 = metadata !{i32 786484, i32 0, null, metadata !"Positive_RA_Alt_Thresh", metadata !"Positive_RA_Alt_Thresh", metadata !"", metadata !5, i32 27, metadata !36, i32 0, i32 1, [4 x i32]* @Positive_RA_Alt_Thresh, null} ; [ DW_TAG_variable ] [Positive_RA_Alt_Thresh] [line 27] [def]
!36 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 128, i64 32, i32 0, i32 0, metadata !11, metadata !37, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 128, align 32, offset 0] [from int]
!37 = metadata !{metadata !38}
!38 = metadata !{i32 786465, i64 0, i64 4}        ; [ DW_TAG_subrange_type ] [0, 3]
!39 = metadata !{i32 786484, i32 0, null, metadata !"Up_Separation", metadata !"Up_Separation", metadata !"", metadata !5, i32 29, metadata !11, i32 0, i32 1, i32* @Up_Separation, null} ; [ DW_TAG_variable ] [Up_Separation] [line 29] [def]
!40 = metadata !{i32 786484, i32 0, null, metadata !"Down_Separation", metadata !"Down_Separation", metadata !"", metadata !5, i32 30, metadata !11, i32 0, i32 1, i32* @Down_Separation, null} ; [ DW_TAG_variable ] [Down_Separation] [line 30] [def]
!41 = metadata !{i32 786484, i32 0, null, metadata !"Other_RAC", metadata !"Other_RAC", metadata !"", metadata !5, i32 33, metadata !11, i32 0, i32 1, i32* @Other_RAC, null} ; [ DW_TAG_variable ] [Other_RAC] [line 33] [def]
!42 = metadata !{i32 786484, i32 0, null, metadata !"Other_Capability", metadata !"Other_Capability", metadata !"", metadata !5, i32 38, metadata !11, i32 0, i32 1, i32* @Other_Capability, null} ; [ DW_TAG_variable ] [Other_Capability] [line 38] [def]
!43 = metadata !{i32 786484, i32 0, null, metadata !"Climb_Inhibit", metadata !"Climb_Inhibit", metadata !"", metadata !5, i32 42, metadata !11, i32 0, i32 1, i32* @Climb_Inhibit, null} ; [ DW_TAG_variable ] [Climb_Inhibit] [line 42] [def]
!44 = metadata !{i32 786449, metadata !45, i32 4, metadata !"clang version 3.5.0 (tags/RELEASE_350/final)", i1 false, metadata !"", i32 0, metadata !46, metadata !59, metadata !104, metadata !119, metadata !139, metadata !"", i32 1} ; [ DW_TAG_compile_unit ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp] [DW_LANG_C_plus_plus]
!45 = metadata !{metadata !"ExecutePathForSmallCase.cpp", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!46 = metadata !{metadata !47}
!47 = metadata !{i32 786436, metadata !48, metadata !49, metadata !"_Ios_Openmode", i32 105, i64 32, i64 32, i32 0, i32 0, null, metadata !51, i32 0, null, null, metadata !"_ZTSSt13_Ios_Openmode"} ; [ DW_TAG_enumeration_type ] [_Ios_Openmode] [line 105, size 32, align 32, offset 0] [def] [from ]
!48 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/bits/ios_base.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!49 = metadata !{i32 786489, metadata !50, null, metadata !"std", i32 171} ; [ DW_TAG_namespace ] [std] [line 171]
!50 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/i386-linux-gnu/c++/4.7/bits/c++config.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!51 = metadata !{metadata !52, metadata !53, metadata !54, metadata !55, metadata !56, metadata !57, metadata !58}
!52 = metadata !{i32 786472, metadata !"_S_app", i64 1} ; [ DW_TAG_enumerator ] [_S_app :: 1]
!53 = metadata !{i32 786472, metadata !"_S_ate", i64 2} ; [ DW_TAG_enumerator ] [_S_ate :: 2]
!54 = metadata !{i32 786472, metadata !"_S_bin", i64 4} ; [ DW_TAG_enumerator ] [_S_bin :: 4]
!55 = metadata !{i32 786472, metadata !"_S_in", i64 8} ; [ DW_TAG_enumerator ] [_S_in :: 8]
!56 = metadata !{i32 786472, metadata !"_S_out", i64 16} ; [ DW_TAG_enumerator ] [_S_out :: 16]
!57 = metadata !{i32 786472, metadata !"_S_trunc", i64 32} ; [ DW_TAG_enumerator ] [_S_trunc :: 32]
!58 = metadata !{i32 786472, metadata !"_S_ios_openmode_end", i64 65536} ; [ DW_TAG_enumerator ] [_S_ios_openmode_end :: 65536]
!59 = metadata !{metadata !60, metadata !65, metadata !67, metadata !68, metadata !80, metadata !86, metadata !88, metadata !90, metadata !92, metadata !97, metadata !47, metadata !102}
!60 = metadata !{i32 786451, metadata !61, null, metadata !"", i32 83, i64 64, i64 32, i32 0, i32 0, null, metadata !62, i32 0, null, null, metadata !"_ZTS11__mbstate_t"} ; [ DW_TAG_structure_type ] [line 83, size 64, align 32, offset 0] [def] [from ]
!61 = metadata !{metadata !"/usr/include/wchar.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!62 = metadata !{metadata !63, metadata !64}
!63 = metadata !{i32 786445, metadata !61, metadata !"_ZTS11__mbstate_t", metadata !"__count", i32 85, i64 32, i64 32, i64 0, i32 0, metadata !11} ; [ DW_TAG_member ] [__count] [line 85, size 32, align 32, offset 0] [from int]
!64 = metadata !{i32 786445, metadata !61, metadata !"_ZTS11__mbstate_t", metadata !"__value", i32 94, i64 32, i64 32, i64 32, i32 0, metadata !"_ZTSN11__mbstate_tUt_E"} ; [ DW_TAG_member ] [__value] [line 94, size 32, align 32, offset 32] [from _ZTSN11__mbstate_tUt_E]
!65 = metadata !{i32 786451, metadata !66, null, metadata !"lconv", i32 54, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTS5lconv"} ; [ DW_TAG_structure_type ] [lconv] [line 54, size 0, align 0, offset 0] [decl] [from ]
!66 = metadata !{metadata !"/usr/include/locale.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!67 = metadata !{i32 786434, metadata !48, metadata !49, metadata !"ios_base", i32 201, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTSSt8ios_base"} ; [ DW_TAG_class_type ] [ios_base] [line 201, size 0, align 0, offset 0] [decl] [from ]
!68 = metadata !{i32 786434, metadata !48, metadata !"_ZTSSt8ios_base", metadata !"Init", i32 535, i64 8, i64 8, i32 0, i32 0, null, metadata !69, i32 0, null, null, metadata !"_ZTSNSt8ios_base4InitE"} ; [ DW_TAG_class_type ] [Init] [line 535, size 8, align 8, offset 0] [def] [from ]
!69 = metadata !{metadata !70, metadata !73, metadata !75, metadata !79}
!70 = metadata !{i32 786445, metadata !48, metadata !"_ZTSNSt8ios_base4InitE", metadata !"_S_refcount", i32 543, i64 0, i64 0, i64 0, i32 4097, metadata !71, null} ; [ DW_TAG_member ] [_S_refcount] [line 543, size 0, align 0, offset 0] [private] [static] [from _Atomic_word]
!71 = metadata !{i32 786454, metadata !72, null, metadata !"_Atomic_word", i32 32, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ] [_Atomic_word] [line 32, size 0, align 0, offset 0] [from int]
!72 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/i386-linux-gnu/c++/4.7/bits/atomic_word.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!73 = metadata !{i32 786445, metadata !48, metadata !"_ZTSNSt8ios_base4InitE", metadata !"_S_synced_with_stdio", i32 544, i64 0, i64 0, i64 0, i32 4097, metadata !74, null} ; [ DW_TAG_member ] [_S_synced_with_stdio] [line 544, size 0, align 0, offset 0] [private] [static] [from bool]
!74 = metadata !{i32 786468, null, null, metadata !"bool", i32 0, i64 8, i64 8, i64 0, i32 0, i32 2} ; [ DW_TAG_base_type ] [bool] [line 0, size 8, align 8, offset 0, enc DW_ATE_boolean]
!75 = metadata !{i32 786478, metadata !48, metadata !"_ZTSNSt8ios_base4InitE", metadata !"Init", metadata !"Init", metadata !"", i32 539, metadata !76, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, null, i32 539} ; [ DW_TAG_subprogram ] [line 539] [Init]
!76 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !77, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!77 = metadata !{null, metadata !78}
!78 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 1088, metadata !"_ZTSNSt8ios_base4InitE"} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [artificial] [from _ZTSNSt8ios_base4InitE]
!79 = metadata !{i32 786478, metadata !48, metadata !"_ZTSNSt8ios_base4InitE", metadata !"~Init", metadata !"~Init", metadata !"", i32 540, metadata !76, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, null, i32 540} ; [ DW_TAG_subprogram ] [line 540] [~Init]
!80 = metadata !{i32 786455, metadata !61, metadata !"_ZTS11__mbstate_t", metadata !"", i32 86, i64 32, i64 32, i64 0, i32 0, null, metadata !81, i32 0, null, null, metadata !"_ZTSN11__mbstate_tUt_E"} ; [ DW_TAG_union_type ] [line 86, size 32, align 32, offset 0] [def] [from ]
!81 = metadata !{metadata !82, metadata !84}
!82 = metadata !{i32 786445, metadata !61, metadata !"_ZTSN11__mbstate_tUt_E", metadata !"__wch", i32 89, i64 32, i64 32, i64 0, i32 0, metadata !83} ; [ DW_TAG_member ] [__wch] [line 89, size 32, align 32, offset 0] [from unsigned int]
!83 = metadata !{i32 786468, null, null, metadata !"unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!84 = metadata !{i32 786445, metadata !61, metadata !"_ZTSN11__mbstate_tUt_E", metadata !"__wchb", i32 93, i64 32, i64 8, i64 0, i32 0, metadata !85} ; [ DW_TAG_member ] [__wchb] [line 93, size 32, align 8, offset 0] [from ]
!85 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 32, i64 8, i32 0, i32 0, metadata !26, metadata !37, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 32, align 8, offset 0] [from char]
!86 = metadata !{i32 786451, metadata !87, null, metadata !"_IO_FILE", i32 273, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTS8_IO_FILE"} ; [ DW_TAG_structure_type ] [_IO_FILE] [line 273, size 0, align 0, offset 0] [decl] [from ]
!87 = metadata !{metadata !"/usr/include/libio.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!88 = metadata !{i32 786451, metadata !89, null, metadata !"", i32 22, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTS9_G_fpos_t"} ; [ DW_TAG_structure_type ] [line 22, size 0, align 0, offset 0] [decl] [from ]
!89 = metadata !{metadata !"/usr/include/_G_config.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!90 = metadata !{i32 786451, metadata !91, null, metadata !"", i32 98, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTS5div_t"} ; [ DW_TAG_structure_type ] [line 98, size 0, align 0, offset 0] [decl] [from ]
!91 = metadata !{metadata !"/usr/include/stdlib.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!92 = metadata !{i32 786451, metadata !91, null, metadata !"", i32 106, i64 64, i64 32, i32 0, i32 0, null, metadata !93, i32 0, null, null, metadata !"_ZTS6ldiv_t"} ; [ DW_TAG_structure_type ] [line 106, size 64, align 32, offset 0] [def] [from ]
!93 = metadata !{metadata !94, metadata !96}
!94 = metadata !{i32 786445, metadata !91, metadata !"_ZTS6ldiv_t", metadata !"quot", i32 108, i64 32, i64 32, i64 0, i32 0, metadata !95} ; [ DW_TAG_member ] [quot] [line 108, size 32, align 32, offset 0] [from long int]
!95 = metadata !{i32 786468, null, null, metadata !"long int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long int] [line 0, size 32, align 32, offset 0, enc DW_ATE_signed]
!96 = metadata !{i32 786445, metadata !91, metadata !"_ZTS6ldiv_t", metadata !"rem", i32 109, i64 32, i64 32, i64 32, i32 0, metadata !95} ; [ DW_TAG_member ] [rem] [line 109, size 32, align 32, offset 32] [from long int]
!97 = metadata !{i32 786451, metadata !91, null, metadata !"", i32 118, i64 128, i64 32, i32 0, i32 0, null, metadata !98, i32 0, null, null, metadata !"_ZTS7lldiv_t"} ; [ DW_TAG_structure_type ] [line 118, size 128, align 32, offset 0] [def] [from ]
!98 = metadata !{metadata !99, metadata !101}
!99 = metadata !{i32 786445, metadata !91, metadata !"_ZTS7lldiv_t", metadata !"quot", i32 120, i64 64, i64 32, i64 0, i32 0, metadata !100} ; [ DW_TAG_member ] [quot] [line 120, size 64, align 32, offset 0] [from long long int]
!100 = metadata !{i32 786468, null, null, metadata !"long long int", i32 0, i64 64, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ] [long long int] [line 0, size 64, align 32, offset 0, enc DW_ATE_signed]
!101 = metadata !{i32 786445, metadata !91, metadata !"_ZTS7lldiv_t", metadata !"rem", i32 121, i64 64, i64 32, i64 64, i32 0, metadata !100} ; [ DW_TAG_member ] [rem] [line 121, size 64, align 32, offset 64] [from long long int]
!102 = metadata !{i32 786434, metadata !103, metadata !49, metadata !"basic_ofstream<char, std::char_traits<char> >", i32 970, i64 0, i64 0, i32 0, i32 4, null, null, i32 0, null, null, metadata !"_ZTSSt14basic_ofstreamIcSt11char_traitsIcEE"} ; [ DW_TAG_class_type ] [basic_ofstream<char, std::char_traits<char> >] [line 970, size 0, align 0, offset 0] [decl] [from ]
!103 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/bits/fstream.tcc", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!104 = metadata !{metadata !105, metadata !108, metadata !110, metadata !113, metadata !116, metadata !117}
!105 = metadata !{i32 786478, metadata !106, metadata !107, metadata !"__cxx_global_var_init", metadata !"__cxx_global_var_init", metadata !"", i32 75, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__cxx_global_var_init, null, null, metadata !2, i32 75} ; [ DW_TAG_subprogram ] [line 75] [local] [def] [__cxx_global_var_init]
!106 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/iostream", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!107 = metadata !{i32 786473, metadata !106}      ; [ DW_TAG_file_type ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib//usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/iostream]
!108 = metadata !{i32 786478, metadata !45, metadata !109, metadata !"__cxx_global_var_init1", metadata !"__cxx_global_var_init1", metadata !"", i32 22, metadata !6, i1 true, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @__cxx_global_var_init1, null, null, metadata !2, i32 22} ; [ DW_TAG_subprogram ] [line 22] [local] [def] [__cxx_global_var_init1]
!109 = metadata !{i32 786473, metadata !45}       ; [ DW_TAG_file_type ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!110 = metadata !{i32 786478, metadata !45, metadata !109, metadata !"recordBasicBlock", metadata !"recordBasicBlock", metadata !"", i32 33, metadata !111, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 (i32, i32)* @recordBasicBlock, null, null, metadata !2, i32 33} ; [ DW_TAG_subprogram ] [line 33] [def] [recordBasicBlock]
!111 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !112, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!112 = metadata !{metadata !11, metadata !11, metadata !11}
!113 = metadata !{i32 786478, metadata !45, metadata !109, metadata !"printPath", metadata !"printPath", metadata !"", i32 56, metadata !114, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32)* @printPath, null, null, metadata !2, i32 56} ; [ DW_TAG_subprogram ] [line 56] [def] [printPath]
!114 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !115, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!115 = metadata !{null, metadata !11}
!116 = metadata !{i32 786478, metadata !45, metadata !109, metadata !"savePath", metadata !"savePath", metadata !"", i32 64, metadata !6, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void ()* @savePath, null, null, metadata !2, i32 64} ; [ DW_TAG_subprogram ] [line 64] [def] [savePath]
!117 = metadata !{i32 786478, metadata !45, metadata !109, metadata !"", metadata !"", metadata !"_GLOBAL__sub_I_ExecutePathForSmallCase.cpp", i32 0, metadata !118, i1 true, i1 true, i32 0, i32 0, null, i32 64, i1 false, void ()* @_GLOBAL__sub_I_ExecutePathForSmallCase.cpp, null, null, metadata !2, i32 0} ; [ DW_TAG_subprogram ] [line 0] [local] [def]
!118 = metadata !{i32 786453, i32 0, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !2, i32 0, null, null, null} ; [ DW_TAG_subroutine_type ] [line 0, size 0, align 0, offset 0] [from ]
!119 = metadata !{metadata !120, metadata !121, metadata !126, metadata !129, metadata !133, metadata !134, metadata !135, metadata !136, metadata !138}
!120 = metadata !{i32 786484, i32 0, metadata !49, metadata !"__ioinit", metadata !"__ioinit", metadata !"_ZStL8__ioinit", metadata !107, i32 75, metadata !"_ZTSNSt8ios_base4InitE", i32 1, i32 1, %"class.std::ios_base::Init"* @_ZStL8__ioinit, null} ; [ DW_TAG_variable ] [__ioinit] [line 75] [local] [def]
!121 = metadata !{i32 786484, i32 0, metadata !67, metadata !"app", metadata !"app", metadata !"", metadata !122, i32 366, metadata !123, i32 1, i32 1, i32 1, metadata !125} ; [ DW_TAG_variable ] [app] [line 366] [local] [def]
!122 = metadata !{i32 786473, metadata !48}       ; [ DW_TAG_file_type ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib//usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/bits/ios_base.h]
!123 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !124} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from openmode]
!124 = metadata !{i32 786454, metadata !48, metadata !"_ZTSSt8ios_base", metadata !"openmode", i32 363, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTSSt13_Ios_Openmode"} ; [ DW_TAG_typedef ] [openmode] [line 363, size 0, align 0, offset 0] [from _ZTSSt13_Ios_Openmode]
!125 = metadata !{i32 786445, metadata !48, metadata !"_ZTSSt8ios_base", metadata !"app", i32 366, i64 0, i64 0, i64 0, i32 4096, metadata !123, i32 1} ; [ DW_TAG_member ] [app] [line 366, size 0, align 0, offset 0] [static] [from ]
!126 = metadata !{i32 786484, i32 0, null, metadata !"out", metadata !"out", metadata !"", metadata !109, i32 22, metadata !127, i32 0, i32 1, %"class.std::basic_ofstream"* @out, null} ; [ DW_TAG_variable ] [out] [line 22] [def]
!127 = metadata !{i32 786454, metadata !128, metadata !49, metadata !"ofstream", i32 162, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTSSt14basic_ofstreamIcSt11char_traitsIcEE"} ; [ DW_TAG_typedef ] [ofstream] [line 162, size 0, align 0, offset 0] [from _ZTSSt14basic_ofstreamIcSt11char_traitsIcEE]
!128 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/iosfwd", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!129 = metadata !{i32 786484, i32 0, null, metadata !"path", metadata !"path", metadata !"", metadata !109, i32 25, metadata !130, i32 0, i32 1, [1000000 x i32]* @path, null} ; [ DW_TAG_variable ] [path] [line 25] [def]
!130 = metadata !{i32 786433, null, null, metadata !"", i32 0, i64 32000000, i64 32, i32 0, i32 0, metadata !11, metadata !131, i32 0, null, null, null} ; [ DW_TAG_array_type ] [line 0, size 32000000, align 32, offset 0] [from int]
!131 = metadata !{metadata !132}
!132 = metadata !{i32 786465, i64 0, i64 1000000} ; [ DW_TAG_subrange_type ] [0, 999999]
!133 = metadata !{i32 786484, i32 0, null, metadata !"_subpathLength", metadata !"_subpathLength", metadata !"", metadata !109, i32 26, metadata !11, i32 0, i32 1, i32* @_subpathLength, null} ; [ DW_TAG_variable ] [_subpathLength] [line 26] [def]
!134 = metadata !{i32 786484, i32 0, null, metadata !"count", metadata !"count", metadata !"", metadata !109, i32 27, metadata !11, i32 0, i32 1, i32* @count, null} ; [ DW_TAG_variable ] [count] [line 27] [def]
!135 = metadata !{i32 786484, i32 0, metadata !110, metadata !"firstBlock", metadata !"firstBlock", metadata !"", metadata !109, i32 35, metadata !74, i32 1, i32 1, i8* @_ZZ16recordBasicBlockE10firstBlock, null} ; [ DW_TAG_variable ] [firstBlock] [line 35] [local] [def]
!136 = metadata !{i32 786484, i32 0, null, metadata !"MAXNUM", metadata !"MAXNUM", metadata !"", metadata !109, i32 24, metadata !137, i32 1, i32 1, i32 1000000, null} ; [ DW_TAG_variable ] [MAXNUM] [line 24] [local] [def]
!137 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from int]
!138 = metadata !{i32 786484, i32 0, metadata !110, metadata !"n_MAXNUM", metadata !"n_MAXNUM", metadata !"", metadata !109, i32 46, metadata !11, i32 1, i32 1, i32* @_ZZ16recordBasicBlockE8n_MAXNUM, null} ; [ DW_TAG_variable ] [n_MAXNUM] [line 46] [local] [def]
!139 = metadata !{metadata !140, metadata !143, metadata !146, metadata !150, metadata !151, metadata !155, metadata !157, metadata !164, metadata !167, metadata !168, metadata !171, metadata !174, metadata !175, metadata !177, metadata !179, metadata !181, metadata !182}
!140 = metadata !{i32 786440, metadata !49, metadata !141, i32 66} ; [ DW_TAG_imported_declaration ]
!141 = metadata !{i32 786454, metadata !61, null, metadata !"mbstate_t", i32 106, i64 0, i64 0, i64 0, i32 0, metadata !142} ; [ DW_TAG_typedef ] [mbstate_t] [line 106, size 0, align 0, offset 0] [from __mbstate_t]
!142 = metadata !{i32 786454, metadata !61, null, metadata !"__mbstate_t", i32 95, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS11__mbstate_t"} ; [ DW_TAG_typedef ] [__mbstate_t] [line 95, size 0, align 0, offset 0] [from _ZTS11__mbstate_t]
!143 = metadata !{i32 786440, metadata !49, metadata !144, i32 141} ; [ DW_TAG_imported_declaration ]
!144 = metadata !{i32 786454, metadata !145, null, metadata !"wint_t", i32 141, i64 0, i64 0, i64 0, i32 0, metadata !83} ; [ DW_TAG_typedef ] [wint_t] [line 141, size 0, align 0, offset 0] [from unsigned int]
!145 = metadata !{metadata !"/home/zhouyan/work/llvm-3.5.0.src/Release+Asserts/bin/../lib/clang/3.5.0/include/stddef.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!146 = metadata !{i32 786490, metadata !147, metadata !149, i32 57} ; [ DW_TAG_imported_module ]
!147 = metadata !{i32 786489, metadata !148, null, metadata !"__gnu_debug", i32 55} ; [ DW_TAG_namespace ] [__gnu_debug] [line 55]
!148 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/debug/debug.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!149 = metadata !{i32 786489, metadata !148, metadata !49, metadata !"__debug", i32 49} ; [ DW_TAG_namespace ] [__debug] [line 49]
!150 = metadata !{i32 786440, metadata !49, metadata !"_ZTS5lconv", i32 55} ; [ DW_TAG_imported_declaration ]
!151 = metadata !{i32 786440, metadata !152, metadata !154, i32 42} ; [ DW_TAG_imported_declaration ]
!152 = metadata !{i32 786489, metadata !153, null, metadata !"__gnu_cxx", i32 243} ; [ DW_TAG_namespace ] [__gnu_cxx] [line 243]
!153 = metadata !{metadata !"/usr/lib/gcc/i686-linux-gnu/4.7/../../../../include/c++/4.7/cwchar", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!154 = metadata !{i32 786454, metadata !50, metadata !49, metadata !"size_t", i32 173, i64 0, i64 0, i64 0, i32 0, metadata !83} ; [ DW_TAG_typedef ] [size_t] [line 173, size 0, align 0, offset 0] [from unsigned int]
!155 = metadata !{i32 786440, metadata !152, metadata !156, i32 43} ; [ DW_TAG_imported_declaration ]
!156 = metadata !{i32 786454, metadata !50, metadata !49, metadata !"ptrdiff_t", i32 174, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ] [ptrdiff_t] [line 174, size 0, align 0, offset 0] [from int]
!157 = metadata !{i32 786440, metadata !49, metadata !158, i32 84} ; [ DW_TAG_imported_declaration ]
!158 = metadata !{i32 786454, metadata !159, null, metadata !"wctrans_t", i32 187, i64 0, i64 0, i64 0, i32 0, metadata !160} ; [ DW_TAG_typedef ] [wctrans_t] [line 187, size 0, align 0, offset 0] [from ]
!159 = metadata !{metadata !"/usr/include/wctype.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!160 = metadata !{i32 786447, null, null, metadata !"", i32 0, i64 32, i64 32, i64 0, i32 0, metadata !161} ; [ DW_TAG_pointer_type ] [line 0, size 32, align 32, offset 0] [from ]
!161 = metadata !{i32 786470, null, null, metadata !"", i32 0, i64 0, i64 0, i64 0, i32 0, metadata !162} ; [ DW_TAG_const_type ] [line 0, size 0, align 0, offset 0] [from __int32_t]
!162 = metadata !{i32 786454, metadata !163, null, metadata !"__int32_t", i32 41, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ] [__int32_t] [line 41, size 0, align 0, offset 0] [from int]
!163 = metadata !{metadata !"/usr/include/i386-linux-gnu/bits/types.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!164 = metadata !{i32 786440, metadata !49, metadata !165, i32 85} ; [ DW_TAG_imported_declaration ]
!165 = metadata !{i32 786454, metadata !159, null, metadata !"wctype_t", i32 53, i64 0, i64 0, i64 0, i32 0, metadata !166} ; [ DW_TAG_typedef ] [wctype_t] [line 53, size 0, align 0, offset 0] [from long unsigned int]
!166 = metadata !{i32 786468, null, null, metadata !"long unsigned int", i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ] [long unsigned int] [line 0, size 32, align 32, offset 0, enc DW_ATE_unsigned]
!167 = metadata !{i32 786440, metadata !49, metadata !144, i32 86} ; [ DW_TAG_imported_declaration ]
!168 = metadata !{i32 786440, metadata !49, metadata !169, i32 97} ; [ DW_TAG_imported_declaration ]
!169 = metadata !{i32 786454, metadata !170, null, metadata !"FILE", i32 49, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS8_IO_FILE"} ; [ DW_TAG_typedef ] [FILE] [line 49, size 0, align 0, offset 0] [from _ZTS8_IO_FILE]
!170 = metadata !{metadata !"/usr/include/stdio.h", metadata !"/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib"}
!171 = metadata !{i32 786440, metadata !49, metadata !172, i32 98} ; [ DW_TAG_imported_declaration ]
!172 = metadata !{i32 786454, metadata !170, null, metadata !"fpos_t", i32 111, i64 0, i64 0, i64 0, i32 0, metadata !173} ; [ DW_TAG_typedef ] [fpos_t] [line 111, size 0, align 0, offset 0] [from _G_fpos_t]
!173 = metadata !{i32 786454, metadata !89, null, metadata !"_G_fpos_t", i32 26, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS9_G_fpos_t"} ; [ DW_TAG_typedef ] [_G_fpos_t] [line 26, size 0, align 0, offset 0] [from _ZTS9_G_fpos_t]
!174 = metadata !{i32 786490, metadata !44, metadata !49, i32 4} ; [ DW_TAG_imported_module ]
!175 = metadata !{i32 786440, metadata !49, metadata !176, i32 102} ; [ DW_TAG_imported_declaration ]
!176 = metadata !{i32 786454, metadata !91, null, metadata !"div_t", i32 102, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS5div_t"} ; [ DW_TAG_typedef ] [div_t] [line 102, size 0, align 0, offset 0] [from _ZTS5div_t]
!177 = metadata !{i32 786440, metadata !49, metadata !178, i32 103} ; [ DW_TAG_imported_declaration ]
!178 = metadata !{i32 786454, metadata !91, null, metadata !"ldiv_t", i32 110, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS6ldiv_t"} ; [ DW_TAG_typedef ] [ldiv_t] [line 110, size 0, align 0, offset 0] [from _ZTS6ldiv_t]
!179 = metadata !{i32 786440, metadata !152, metadata !180, i32 165} ; [ DW_TAG_imported_declaration ]
!180 = metadata !{i32 786454, metadata !91, null, metadata !"lldiv_t", i32 122, i64 0, i64 0, i64 0, i32 0, metadata !"_ZTS7lldiv_t"} ; [ DW_TAG_typedef ] [lldiv_t] [line 122, size 0, align 0, offset 0] [from _ZTS7lldiv_t]
!181 = metadata !{i32 786440, metadata !49, metadata !180, i32 208} ; [ DW_TAG_imported_declaration ]
!182 = metadata !{i32 786490, metadata !44, metadata !49, i32 11} ; [ DW_TAG_imported_module ]
!183 = metadata !{i32 2, metadata !"Dwarf Version", i32 4}
!184 = metadata !{i32 2, metadata !"Debug Info Version", i32 1}
!185 = metadata !{metadata !"clang version 3.5.0 (tags/RELEASE_350/final)"}
!186 = metadata !{i32 50, i32 5, metadata !4, null}
!187 = metadata !{i32 51, i32 5, metadata !4, null}
!188 = metadata !{i32 52, i32 5, metadata !4, null}
!189 = metadata !{i32 53, i32 5, metadata !4, null}
!190 = metadata !{i32 54, i32 1, metadata !4, null}
!191 = metadata !{i32 58, i32 2, metadata !8, null} ; [ DW_TAG_imported_module ]
!192 = metadata !{i32 63, i32 5, metadata !12, null}
!193 = metadata !{i32 63, i32 5, metadata !194, null}
!194 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 1, i32 13} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!195 = metadata !{i32 63, i32 5, metadata !196, null}
!196 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 2, i32 14} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!197 = metadata !{i32 63, i32 5, metadata !198, null}
!198 = metadata !{i32 786443, metadata !1, metadata !199, i32 63, i32 5, i32 4, i32 16} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!199 = metadata !{i32 786443, metadata !1, metadata !12, i32 63, i32 5, i32 3, i32 15} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!200 = metadata !{i32 786688, metadata !13, metadata !"upward_preferred", metadata !5, i32 68, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_preferred] [line 68]
!201 = metadata !{i32 68, i32 9, metadata !13, null}
!202 = metadata !{i32 786688, metadata !13, metadata !"upward_crossing_situation", metadata !5, i32 69, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_crossing_situation] [line 69]
!203 = metadata !{i32 69, i32 9, metadata !13, null}
!204 = metadata !{i32 786688, metadata !13, metadata !"result", metadata !5, i32 70, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [result] [line 70]
!205 = metadata !{i32 70, i32 10, metadata !13, null}
!206 = metadata !{i32 72, i32 24, metadata !13, null}
!207 = metadata !{i32 73, i32 9, metadata !208, null}
!208 = metadata !{i32 786443, metadata !1, metadata !13, i32 73, i32 9, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!209 = metadata !{i32 75, i32 13, metadata !210, null}
!210 = metadata !{i32 786443, metadata !1, metadata !208, i32 74, i32 5, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!211 = metadata !{i32 75, i32 38, metadata !212, null}
!212 = metadata !{i32 786443, metadata !1, metadata !210, i32 75, i32 38, i32 1, i32 17} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!213 = metadata !{i32 75, i32 38, metadata !214, null}
!214 = metadata !{i32 786443, metadata !1, metadata !210, i32 75, i32 38, i32 3, i32 19} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!215 = metadata !{i32 75, i32 83, metadata !210, null}
!216 = metadata !{i32 75, i32 83, metadata !217, null}
!217 = metadata !{i32 786443, metadata !1, metadata !210, i32 75, i32 83, i32 4, i32 20} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!218 = metadata !{i32 75, i32 83, metadata !219, null}
!219 = metadata !{i32 786443, metadata !1, metadata !220, i32 75, i32 83, i32 5, i32 21} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!220 = metadata !{i32 786443, metadata !1, metadata !210, i32 75, i32 83, i32 2, i32 18} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!221 = metadata !{i32 76, i32 5, metadata !210, null}
!222 = metadata !{i32 79, i32 11, metadata !223, null}
!223 = metadata !{i32 786443, metadata !1, metadata !208, i32 78, i32 5, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!224 = metadata !{i32 79, i32 11, metadata !225, null}
!225 = metadata !{i32 786443, metadata !1, metadata !223, i32 79, i32 11, i32 1, i32 22} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!226 = metadata !{i32 79, i32 11, metadata !227, null}
!227 = metadata !{i32 786443, metadata !1, metadata !223, i32 79, i32 11, i32 3, i32 24} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!228 = metadata !{i32 79, i32 83, metadata !223, null}
!229 = metadata !{i32 79, i32 83, metadata !230, null}
!230 = metadata !{i32 786443, metadata !1, metadata !231, i32 79, i32 83, i32 4, i32 25} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!231 = metadata !{i32 786443, metadata !1, metadata !223, i32 79, i32 83, i32 2, i32 23} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!232 = metadata !{i32 81, i32 5, metadata !13, null}
!233 = metadata !{i32 786688, metadata !17, metadata !"upward_preferred", metadata !5, i32 86, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_preferred] [line 86]
!234 = metadata !{i32 86, i32 9, metadata !17, null}
!235 = metadata !{i32 786688, metadata !17, metadata !"upward_crossing_situation", metadata !5, i32 87, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [upward_crossing_situation] [line 87]
!236 = metadata !{i32 87, i32 9, metadata !17, null}
!237 = metadata !{i32 786688, metadata !17, metadata !"result", metadata !5, i32 88, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [result] [line 88]
!238 = metadata !{i32 88, i32 10, metadata !17, null}
!239 = metadata !{i32 90, i32 24, metadata !17, null}
!240 = metadata !{i32 91, i32 9, metadata !241, null}
!241 = metadata !{i32 786443, metadata !1, metadata !17, i32 91, i32 9, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!242 = metadata !{i32 93, i32 11, metadata !243, null}
!243 = metadata !{i32 786443, metadata !1, metadata !241, i32 92, i32 5, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!244 = metadata !{i32 93, i32 11, metadata !245, null}
!245 = metadata !{i32 786443, metadata !1, metadata !243, i32 93, i32 11, i32 1, i32 26} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!246 = metadata !{i32 93, i32 11, metadata !247, null}
!247 = metadata !{i32 786443, metadata !1, metadata !243, i32 93, i32 11, i32 3, i32 28} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!248 = metadata !{i32 93, i32 85, metadata !243, null}
!249 = metadata !{i32 93, i32 85, metadata !250, null}
!250 = metadata !{i32 786443, metadata !1, metadata !251, i32 93, i32 85, i32 4, i32 29} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!251 = metadata !{i32 786443, metadata !1, metadata !243, i32 93, i32 85, i32 2, i32 27} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!252 = metadata !{i32 94, i32 5, metadata !243, null}
!253 = metadata !{i32 97, i32 13, metadata !254, null}
!254 = metadata !{i32 786443, metadata !1, metadata !241, i32 96, i32 5, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!255 = metadata !{i32 97, i32 38, metadata !256, null}
!256 = metadata !{i32 786443, metadata !1, metadata !254, i32 97, i32 38, i32 1, i32 30} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!257 = metadata !{i32 97, i32 38, metadata !258, null}
!258 = metadata !{i32 786443, metadata !1, metadata !254, i32 97, i32 38, i32 3, i32 32} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!259 = metadata !{i32 97, i32 79, metadata !254, null}
!260 = metadata !{i32 97, i32 79, metadata !261, null}
!261 = metadata !{i32 786443, metadata !1, metadata !254, i32 97, i32 79, i32 4, i32 33} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!262 = metadata !{i32 97, i32 79, metadata !263, null}
!263 = metadata !{i32 786443, metadata !1, metadata !264, i32 97, i32 79, i32 5, i32 34} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!264 = metadata !{i32 786443, metadata !1, metadata !254, i32 97, i32 79, i32 2, i32 31} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!265 = metadata !{i32 99, i32 5, metadata !17, null}
!266 = metadata !{i32 104, i32 5, metadata !18, null}
!267 = metadata !{i32 109, i32 5, metadata !19, null}
!268 = metadata !{i32 786688, metadata !20, metadata !"enabled", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [enabled] [line 114]
!269 = metadata !{i32 114, i32 10, metadata !20, null}
!270 = metadata !{i32 786688, metadata !20, metadata !"tcas_equipped", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [tcas_equipped] [line 114]
!271 = metadata !{i32 114, i32 19, metadata !20, null}
!272 = metadata !{i32 786688, metadata !20, metadata !"intent_not_known", metadata !5, i32 114, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [intent_not_known] [line 114]
!273 = metadata !{i32 114, i32 34, metadata !20, null}
!274 = metadata !{i32 786688, metadata !20, metadata !"need_upward_RA", metadata !5, i32 115, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [need_upward_RA] [line 115]
!275 = metadata !{i32 115, i32 10, metadata !20, null}
!276 = metadata !{i32 786688, metadata !20, metadata !"need_downward_RA", metadata !5, i32 115, metadata !16, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [need_downward_RA] [line 115]
!277 = metadata !{i32 115, i32 26, metadata !20, null}
!278 = metadata !{i32 786688, metadata !20, metadata !"alt_sep", metadata !5, i32 116, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [alt_sep] [line 116]
!279 = metadata !{i32 116, i32 9, metadata !20, null}
!280 = metadata !{i32 118, i32 5, metadata !20, null}
!281 = metadata !{i32 118, i32 5, metadata !282, null}
!282 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 1, i32 35} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!283 = metadata !{i32 118, i32 5, metadata !284, null}
!284 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 3, i32 37} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!285 = metadata !{i32 118, i32 5, metadata !286, null}
!286 = metadata !{i32 786443, metadata !1, metadata !287, i32 118, i32 5, i32 4, i32 38} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!287 = metadata !{i32 786443, metadata !1, metadata !20, i32 118, i32 5, i32 2, i32 36} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!288 = metadata !{i32 119, i32 5, metadata !20, null}
!289 = metadata !{i32 120, i32 5, metadata !20, null}
!290 = metadata !{i32 120, i32 5, metadata !291, null}
!291 = metadata !{i32 786443, metadata !1, metadata !20, i32 120, i32 5, i32 1, i32 39} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!292 = metadata !{i32 120, i32 5, metadata !293, null}
!293 = metadata !{i32 786443, metadata !1, metadata !20, i32 120, i32 5, i32 2, i32 40} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!294 = metadata !{i32 122, i32 5, metadata !20, null}
!295 = metadata !{i32 124, i32 9, metadata !296, null}
!296 = metadata !{i32 786443, metadata !1, metadata !20, i32 124, i32 9, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!297 = metadata !{i32 124, i32 9, metadata !298, null}
!298 = metadata !{i32 786443, metadata !1, metadata !296, i32 124, i32 9, i32 1, i32 41} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!299 = metadata !{i32 124, i32 9, metadata !300, null}
!300 = metadata !{i32 786443, metadata !1, metadata !296, i32 124, i32 9, i32 2, i32 42} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!301 = metadata !{i32 124, i32 9, metadata !302, null}
!302 = metadata !{i32 786443, metadata !1, metadata !303, i32 124, i32 9, i32 4, i32 44} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!303 = metadata !{i32 786443, metadata !1, metadata !296, i32 124, i32 9, i32 3, i32 43} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!304 = metadata !{i32 126, i32 19, metadata !305, null}
!305 = metadata !{i32 786443, metadata !1, metadata !296, i32 125, i32 5, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!306 = metadata !{i32 126, i32 50, metadata !307, null}
!307 = metadata !{i32 786443, metadata !1, metadata !305, i32 126, i32 50, i32 1, i32 45} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!308 = metadata !{i32 126, i32 50, metadata !309, null}
!309 = metadata !{i32 786443, metadata !1, metadata !305, i32 126, i32 50, i32 2, i32 46} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!310 = metadata !{i32 127, i32 21, metadata !305, null}
!311 = metadata !{i32 127, i32 54, metadata !312, null}
!312 = metadata !{i32 786443, metadata !1, metadata !305, i32 127, i32 54, i32 1, i32 47} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!313 = metadata !{i32 127, i32 54, metadata !314, null}
!314 = metadata !{i32 786443, metadata !1, metadata !305, i32 127, i32 54, i32 2, i32 48} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!315 = metadata !{i32 128, i32 6, metadata !316, null}
!316 = metadata !{i32 786443, metadata !1, metadata !305, i32 128, i32 6, i32 0, i32 8} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!317 = metadata !{i32 128, i32 6, metadata !318, null}
!318 = metadata !{i32 786443, metadata !1, metadata !316, i32 128, i32 6, i32 1, i32 49} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!319 = metadata !{i32 132, i32 6, metadata !316, null}
!320 = metadata !{i32 133, i32 11, metadata !321, null}
!321 = metadata !{i32 786443, metadata !1, metadata !316, i32 133, i32 11, i32 0, i32 9} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!322 = metadata !{i32 134, i32 6, metadata !321, null}
!323 = metadata !{i32 135, i32 11, metadata !324, null}
!324 = metadata !{i32 786443, metadata !1, metadata !321, i32 135, i32 11, i32 0, i32 10} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!325 = metadata !{i32 136, i32 6, metadata !324, null}
!326 = metadata !{i32 138, i32 6, metadata !324, null}
!327 = metadata !{i32 139, i32 5, metadata !305, null}
!328 = metadata !{i32 141, i32 5, metadata !20, null}
!329 = metadata !{i32 786689, metadata !21, metadata !"argc", metadata !5, i32 16777360, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [argc] [line 144]
!330 = metadata !{i32 144, i32 15, metadata !21, null}
!331 = metadata !{i32 786689, metadata !21, metadata !"argv", metadata !5, i32 33554576, metadata !24, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [argv] [line 144]
!332 = metadata !{i32 144, i32 27, metadata !21, null}
!333 = metadata !{i32 146, i32 8, metadata !334, null}
!334 = metadata !{i32 786443, metadata !1, metadata !21, i32 146, i32 8, i32 0, i32 11} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!335 = metadata !{i32 148, i32 2, metadata !336, null}
!336 = metadata !{i32 786443, metadata !1, metadata !334, i32 147, i32 5, i32 0, i32 12} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/example/tcas/source/tcas.c]
!337 = metadata !{i32 149, i32 2, metadata !336, null}
!338 = metadata !{i32 150, i32 2, metadata !336, null}
!339 = metadata !{i32 151, i32 2, metadata !336, null}
!340 = metadata !{i32 152, i32 2, metadata !336, null}
!341 = metadata !{i32 153, i32 2, metadata !336, null}
!342 = metadata !{i32 155, i32 5, metadata !21, null}
!343 = metadata !{i32 156, i32 24, metadata !21, null}
!344 = metadata !{i32 157, i32 23, metadata !21, null}
!345 = metadata !{i32 158, i32 34, metadata !21, null}
!346 = metadata !{i32 159, i32 23, metadata !21, null}
!347 = metadata !{i32 160, i32 28, metadata !21, null}
!348 = metadata !{i32 161, i32 25, metadata !21, null}
!349 = metadata !{i32 162, i32 23, metadata !21, null}
!350 = metadata !{i32 163, i32 21, metadata !21, null}
!351 = metadata !{i32 164, i32 23, metadata !21, null}
!352 = metadata !{i32 165, i32 17, metadata !21, null}
!353 = metadata !{i32 166, i32 24, metadata !21, null}
!354 = metadata !{i32 167, i32 21, metadata !21, null}
!355 = metadata !{i32 169, i32 5, metadata !21, null}
!356 = metadata !{i32 169, i32 29, metadata !21, null}
!357 = metadata !{i32 170, i32 5, metadata !21, null}
!358 = metadata !{i32 171, i32 1, metadata !21, null}
!359 = metadata !{i32 786689, metadata !110, metadata !"id", metadata !109, i32 16777249, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [id] [line 33]
!360 = metadata !{i32 33, i32 26, metadata !110, null}
!361 = metadata !{i32 786689, metadata !110, metadata !"subpathLength", metadata !109, i32 33554465, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [subpathLength] [line 33]
!362 = metadata !{i32 33, i32 33, metadata !110, null}
!363 = metadata !{i32 36, i32 5, metadata !364, null}
!364 = metadata !{i32 786443, metadata !45, metadata !110, i32 36, i32 5, i32 0, i32 0} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!365 = metadata !{i32 37, i32 3, metadata !366, null}
!366 = metadata !{i32 786443, metadata !45, metadata !364, i32 36, i32 16, i32 0, i32 1} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!367 = metadata !{i32 38, i32 3, metadata !366, null}
!368 = metadata !{i32 39, i32 3, metadata !366, null}
!369 = metadata !{i32 40, i32 2, metadata !366, null}
!370 = metadata !{i32 42, i32 3, metadata !110, null}
!371 = metadata !{i32 44, i32 5, metadata !372, null}
!372 = metadata !{i32 786443, metadata !45, metadata !110, i32 44, i32 5, i32 0, i32 2} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!373 = metadata !{i32 45, i32 3, metadata !374, null}
!374 = metadata !{i32 786443, metadata !45, metadata !372, i32 44, i32 21, i32 0, i32 3} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!375 = metadata !{i32 47, i32 3, metadata !374, null}
!376 = metadata !{i32 48, i32 3, metadata !374, null}
!377 = metadata !{i32 49, i32 2, metadata !374, null}
!378 = metadata !{i32 50, i32 2, metadata !110, null}
!379 = metadata !{i32 786688, metadata !380, metadata !"i", metadata !109, i32 66, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 66]
!380 = metadata !{i32 786443, metadata !45, metadata !116, i32 66, i32 2, i32 0, i32 6} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!381 = metadata !{i32 66, i32 10, metadata !380, null}
!382 = metadata !{i32 66, i32 6, metadata !380, null}
!383 = metadata !{i32 66, i32 6, metadata !384, null}
!384 = metadata !{i32 786443, metadata !45, metadata !385, i32 66, i32 6, i32 2, i32 9} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!385 = metadata !{i32 786443, metadata !45, metadata !380, i32 66, i32 6, i32 1, i32 8} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!386 = metadata !{i32 67, i32 5, metadata !387, null}
!387 = metadata !{i32 786443, metadata !45, metadata !380, i32 66, i32 30, i32 0, i32 7} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!388 = metadata !{i32 68, i32 2, metadata !387, null}
!389 = metadata !{i32 66, i32 26, metadata !380, null}
!390 = metadata !{i32 69, i32 1, metadata !116, null}
!391 = metadata !{i32 786689, metadata !113, metadata !"currentInput", metadata !109, i32 16777272, metadata !11, i32 0, i32 0} ; [ DW_TAG_arg_variable ] [currentInput] [line 56]
!392 = metadata !{i32 56, i32 20, metadata !113, null}
!393 = metadata !{i32 57, i32 2, metadata !113, null}
!394 = metadata !{i32 786688, metadata !395, metadata !"i", metadata !109, i32 58, metadata !11, i32 0, i32 0} ; [ DW_TAG_auto_variable ] [i] [line 58]
!395 = metadata !{i32 786443, metadata !45, metadata !113, i32 58, i32 2, i32 0, i32 4} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!396 = metadata !{i32 58, i32 10, metadata !395, null} ; [ DW_TAG_imported_module ]
!397 = metadata !{i32 58, i32 6, metadata !395, null} ; [ DW_TAG_imported_module ]
!398 = metadata !{i32 58, i32 6, metadata !399, null} ; [ DW_TAG_imported_module ]
!399 = metadata !{i32 786443, metadata !45, metadata !400, i32 58, i32 6, i32 2, i32 11} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!400 = metadata !{i32 786443, metadata !45, metadata !395, i32 58, i32 6, i32 1, i32 10} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!401 = metadata !{i32 59, i32 4, metadata !402, null}
!402 = metadata !{i32 786443, metadata !45, metadata !395, i32 58, i32 30, i32 0, i32 5} ; [ DW_TAG_lexical_block ] [/home/zhouyan/work/llvm-3.5.0.src/lib/Transforms/length_n_coverage/mylib/ExecutePathForSmallCase.cpp]
!403 = metadata !{i32 60, i32 3, metadata !402, null}
!404 = metadata !{i32 58, i32 26, metadata !395, null} ; [ DW_TAG_imported_module ]
!405 = metadata !{i32 61, i32 3, metadata !113, null}
!406 = metadata !{i32 62, i32 1, metadata !113, null}
!407 = metadata !{i32 75, i32 25, metadata !105, null}
!408 = metadata !{i32 22, i32 10, metadata !108, null}
!409 = metadata !{i32 0, i32 0, metadata !117, null}
