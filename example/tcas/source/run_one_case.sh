#!/bin/bash
result="all_subpath"
n=$2

if [ ! -d $result ];then 
	mkdir all_subpath
fi


#make clean
make SAVEDIR=$result SUBPATHLENGTH=$n LLVM_SRC=$1

echo ">>>>>>>>running a test case: ./tcas.exe  958 1 1 2597  574 4253 0  399  400 0 0 1 "
./tcas.exe  958 1 1 2597  574 4253 0  399  400 0 0 1 

echo ">>>>>>>>calculate length n subpath coverage..."

./coverage $n

echo ">>>>>>>>finished."

echo ">>>>>>>>please check file \"exec_path.txt\" for the execution path and
              directory \"$result\" for the subpath coverages from length-1 to length-$n."

echo $1