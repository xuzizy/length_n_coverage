#define DEBUG_TYPE "length_n_coverage"
#include "llvm/Pass.h" 
#include "llvm/IR/Function.h" 
#include "llvm/Support/raw_ostream.h" 
#include "llvm/Transforms/Scalar.h" 
#include "llvm/Transforms/Utils/BuildLibCalls.h" 
#include "llvm/IR/Intrinsics.h"
#include "llvm/IR/LLVMContext.h" 
#include "llvm/IR/Module.h"  
#include "llvm/Analysis/ValueTracking.h" 
#include "llvm/IR/DebugInfo.h"
#include "llvm/IR/Dominators.h"
//#include "llvm/Target/TargetData.h" 
#include "llvm/Target/TargetLibraryInfo.h" 
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/StringMap.h"
#include "llvm/ADT/Statistic.h" 
#include "llvm/ADT/STLExtras.h" 
#include "llvm/Support/Debug.h"
#include "llvm/Config/config.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/Instruction.h"
#include "llvm/CodeGen/ValueTypes.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/IR/GlobalValue.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/TypeBuilder.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Support/FileSystem.h"
#include <vector>

#include "findLoop.h"
#include "programCFG.h"
#include "convinent.h"
#include "length_n_subpath.h"
using namespace llvm;
using namespace std;
//command line argument int n
cl::opt<int >
subpathLength("n",  
  cl::desc("Subpath length"), cl::value_desc("length"));

/* There are 2 function must be insert to the program.
* 1. i32 recordBasicBlock(i32 id,i32 subpathLength)
*     It is inserted in every basic block,recording the execution path.
*2. i32 coverageOutput()
*		It is inserted in the exit block of the program to output the length-n subpath coverage
* of the current execution or the coverage after running a set of test cases.
*/
void createDeclaration(Module &M){
//	PointerType * int8PtrTy = PointerType::getInt8PtrTy(M.getContext());
	IntegerType *int32Ty = IntegerType::getInt32Ty(M.getContext()); 

	//parameters
	std::vector<Type*>funcTy_param; 
	funcTy_param.push_back(int32Ty);  //argument: i32 
	funcTy_param.push_back(int32Ty);
	ArrayRef<Type*>* params = new ArrayRef<Type*>(funcTy_param);

	FunctionType *functyRecordBB = FunctionType::get(int32Ty,//result
									*params,//parameters
									false);//isVarArg
	//create declaration of i32 recordBasicBlock(i32)
	Function::Create(functyRecordBB,//type
										  GlobalValue::ExternalLinkage,
										  "recordBasicBlock",//function name
										  &M);//Module
	//release memory
	delete params;


	//Create declaration of int atexit(void (*)(void));
	//firstly, create function type void ()(void)
	FunctionType* ft = TypeBuilder<void(void),true>::get(M.getContext());

	//than get function pointer type   void (*)(void)
	PointerType* ptr_ft = PointerType::get(ft,0);

	//finally,create the function type int atexit(void(*)void)
	funcTy_param.clear();
	funcTy_param.push_back(ptr_ft);
	params = new ArrayRef<Type*>(funcTy_param);

	FunctionType* ft_atexit = FunctionType::get(int32Ty,//result
																																												*params,//parameters
																																												false);//isVarArg
	M.getOrInsertFunction("savePath",ft);
	M.getOrInsertFunction("atexit",ft_atexit);
	
 	errs() << *ft<<"\n"<<*ptr_ft<<"\n"<<*ft_atexit<<"\n";
}

 
 

void insertRecordBasicBlock(int id,Function *RecordBasicBlock,Instruction* insertBefore,Module& M) {
	//constructor the argument constant int value  of RecordBasicBlock(int).
 
	Constant *arg_id = ConstantInt::get(IntegerType::getInt32Ty(M.getContext()),
																																						id,true); 
	Constant *arg_subpathLength = ConstantInt::get(IntegerType::getInt32Ty(M.getContext()),
																																						subpathLength,true); 
	vector<Value*> v;
	v.push_back(arg_id);
	v.push_back(arg_subpathLength);
 ArrayRef<Value*> args(v);
	CallInst::Create(RecordBasicBlock, args,"",insertBefore); 
}
 
namespace {
  // length_n_coverage - The first implementation, without getAnalysisUsage.
	struct length_n_coverage : public FunctionPass {
		ProgramCFG* pCFG;
		raw_fd_ostream* FileLoopInfo; 
		static char ID; // Pass identification, replacement for typeid
		length_n_coverage() : FunctionPass(ID) {	
		//Initialize file CFG.BE,
			//raw_fd_ostream f("CFG.BE",errorInfo,sys::fs::F_Text);

			std:: string errorInfo ;
			FileLoopInfo = new raw_fd_ostream ("CFG.BE",errorInfo,sys::fs::F_Text);;
			if(!errorInfo.empty()){
				errs() << "can't write file CFG.BE\n";
				exit(-1); 
			}
		}
		bool doInitialization(Module &M);
		bool runOnFunction(Function &F) ;
	 void getAnalysisUsage(AnalysisUsage &AU) const { 
   AU.addRequired<LoopInfo>(); 
   AU.addPreserved<LoopInfo>(); 
   AU.setPreservesCFG(); 
		}

	};
}

/*
 Tracking execution path in order to calculate the total length-n coverage.
*/
bool length_n_coverage::doInitialization(Module &M){ 
	errs()<<"build programCFG----------------------------------------------------------------------------\n";
			//calculate total length_n_subpath
 
	pCFG = new ProgramCFG(M);
	errs() << "subpathLength = " << subpathLength<<"\n";
	totalSubpath(*pCFG,subpathLength);
	errs()<<"------------------length_n_coverage Pass finished-----------------------------------------------------------------------------\n";


	createDeclaration(M);//create recordBasicBlock(i8*)
	Function *recordBasicBlock = M.getFunction("recordBasicBlock");
	//Function *coverageOutput = M.getFunction("coverageOutput");
	Function *main = M.getFunction("main");
 
 if(!main){
 	errs() << "Can't find main function!\n";
 	exit(-1);
 }


	//Insert recordBasicBlock(id) in every basic block to record execution basic block path.
	std::vector<Value*> args;
	for(Module::iterator fi = M.begin(); fi != M.end(); fi++){
		if(fi->isDeclaration()) continue;
		for (Function::iterator bi = fi->begin(); bi != fi->end(); bi++){
				int id = pCFG->getNode(bi)->id;
				Instruction * insertBefore = bi->getTerminator();

				insertRecordBasicBlock(
										id,
										recordBasicBlock,
										insertBefore, M);
				args.pop_back(); 
		}
	}

	//Insert a function call atexit(savePath)
	Function *func_savePath = M.getFunction("savePath");
	Function *func_atexit = M.getFunction("atexit");
	assert(func_savePath && func_atexit);
	vector<Value*> v;
	v.push_back(func_savePath);
 ArrayRef<Value*> argsRef(v);
 //insert atexit(savePath) in the main 's entry block
	CallInst::Create(func_atexit, argsRef,"",main->getEntryBlock().getTerminator()); 

//errs()<<"------------------Analyse loop-------\n" ;
	//loop and strongly connected component
	//findAllBackedges(M,*pCFG);
 //findAllScc(M,*pCFG);
// errs()<<"-----------------Analyse loop finished-------\n";


 
	return true;
}

bool length_n_coverage::runOnFunction(Function &F) { 
			// write the loop header and loop exit blocks to the file
	 	if(!F.isDeclaration()){
	 		LoopInfo& li = getAnalysis<LoopInfo>(); 

 			//First,get all top level loop in function F.
 			//Each top level loop may contains inner loops.
 			//All loops == top level loops + sub loops of each top level loop
 			//Each loop information is one line in file.
 			//Each line is in the form of:
 			//loopheader exitBlock1 exitBlock2 ......
 			for(LoopInfoBase<BasicBlock, Loop>::iterator iter = li.begin(); 
 				iter != li.end(); iter++){
 				//Get exit blocks of each top level loop
 					SmallVector<BasicBlock*,10>	exitBlocks;
 					(*iter)->getExitBlocks(exitBlocks);
 				 
	 				(*FileLoopInfo) << pCFG->getNode((*iter)->getHeader())->id <<" ";
		 			for(SmallVector<BasicBlock*,10>::iterator exitIter = exitBlocks.begin(); 
		 				exitIter != exitBlocks.end();
		 				exitIter++){ 
		 				(*FileLoopInfo)<< pCFG->getNode(*exitIter)->id <<" "; 
		 			}
		 			(*FileLoopInfo) << "\n";  

 			 //Each top level loop may contains inner loops
 			 //For each sub loop
 				errs() << "sub loop num "<<(*iter)->getSubLoops().size()
 				<< "getNumBlocks "<<(*iter)->getNumBlocks();
 				for(vector<Loop*>::const_iterator loopIter = (*iter)->begin();
 					loopIter != (*iter)->end(); loopIter++ ){
 					//Get exit blocks of each sub loop
 					exitBlocks.clear();
 					SmallVector<BasicBlock*,10>	exitBlocks;
 					(*loopIter)->getExitBlocks(exitBlocks);
 				 
	 				(*FileLoopInfo) << pCFG->getNode((*loopIter)->getHeader())->id <<" ";
		 			for(SmallVector<BasicBlock*,10>::iterator exitIter = exitBlocks.begin(); 
		 				exitIter != exitBlocks.end();
		 				exitIter++){ 
		 				(*FileLoopInfo)<< pCFG->getNode(*exitIter)->id <<" "; 
		 			}
		 			(*FileLoopInfo) << "\n";
	 			}
 			}
 		}
 		FileLoopInfo->flush();
 		errs()<<"finishRunOn"<<F.getName()<<"\n";
	return false;
}
char length_n_coverage::ID = 0;
static RegisterPass<length_n_coverage> X("length_n_coverage", "build program CFG, print current execution BasicBlock path");