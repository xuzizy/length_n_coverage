#nCov:A Tool for Measuring Length-n Subpath Coverage

<<<<<<< HEAD
nCov is a tool  for measuring length-n subpath coverage.This is a tutorial about 
installation and usage of nCov. 
=======
>>>>>>> f53770d3a7e5a7a7ac4f25f7d4b9e8874ce3669d




* [Overview](#overview) 
* [Environment](#environment)
 	* [OS](#os)
 	* [Plateform](#plateform)	
* [Installation](#installation)
 	* [Install_LLVM_and_Clang](#install_llvm_and_clang)
 	* [Install_nCov](#install_ncov)
 	* [Running_A_Demo_Case](#running_a_demo_case)

## Overview

nCov is a tool for measuring length-n subpath coverage. It supports C language. This is a tutorial about installation and usage of nCov. nCov is a [LLVM Pass](http://llvm.org/docs/WritingAnLLVMPass.html).

## Environment
### OS	
* ubuntu 12.04

### Plateform
* [LLVM 3.5](http://llvm.org/releases/3.5.0/docs/GettingStarted.html)

 
## Installation

Since nCov is a LLVM Pass, the installation of nCov is the same as a normal pass.
People first should build the llvm 3.5 and clang 3.5. And then install our tool nCov. 


### Install_LLVM_and_Clang

1.Checkout LLVM:

   * cd where-you-want-llvm-to-live
   * download [llvm 3.5 source code](environment/llvm-3.5.0.src.tar.xz) and
     and copy the src file to the current directory.
   * tar -xf llvm-3.5.0.src.tar.xz

2.Checkout Clang:

   * cd where-you-want-llvm-to-live
   * cd llvm-3.5.0.src/tools
   * download [clang 3.5 source code](environment/cfe-3.5.0.src.tar.xz) 
     and copy the src file to the current directory.
   * tar -xf cfe-3.5.0.src.tar.xz

3.Configure and build LLVM and Clang:

   * cd where-you-want-llvm-to-live
   * mkdir build (for building without polluting the source dir)
   * cd build
   * ../llvm-3.5.0.src/configure [options] Some common options:
     **   --prefix=directory — Specify for directory the full pathname of where you want the LLVM tools and libraries to be installed (default /usr/local).
     **   --enable-optimized — Compile with optimizations enabled (default is NO).
     **  --enable-assertions — Compile with assertion checks enabled (default is YES).
   * make [-j] — The -j specifies the number of jobs (commands) to run simultaneously. This builds both LLVM and Clang for Debug+Asserts mode. The --enable-optimized configure option is used to specify a Release build.
   * make check-all — This run the regression tests to ensure everything is in working order.

Consult the [Getting Started with LLVM](http://llvm.org/releases/3.5.0/docs/GettingStarted.html) section for detailed information on configuring and compiling LLVM.

### Install_nCov
   * cd where-you-want-llvm-to-live
   * cd llvm-3.5.0.src/lib/Transforms
   * git clone https://xuzizy@bitbucket.org/xuzizy/length_n_coverage.git 
   * cd length_n_coverage
   * make clean
   * make        #compile the pass
   * cd coverage #compile the coverage calculation module
   * make clean
   * make

Consult the [Writing An LLVM Pass](http://llvm.org/docs/WritingAnLLVMPass.html) page for detailed information on writing and building a LLVM pass.

## Running_A_Demo_Case

 There is a demo example tcas in our length_n_coverage/example. The program tcas is a program of [Siemens](http://sir.unl.edu/portal/bios/tcas.php) benchmark suites. Compiling,running the program and caluculating the length-n subpath coverage by following steps.
   
   * cd length_n_coverage/example/tcas/source
   * make clean 
   * ./run_one_case.sh *FULL_PATH_TO_LLVM_SRC		N* 
   (For example, in my execution *FULL_PATH_TO_LLVM_SRC*=*/home/zhouyan/work/llvm-3.5.0.src* and *N*=8, you can modify FULL_PATH_TO_LLVM_SRC and set *N* by yourself. )


   The script run_one_case.sh is to compilation and execution a simple test case. You can see the following information after perform above steps:

	>>>>>>>>running a test case
	0
	>>>>>>>>calculate length n subpath coverage...
	all_subpath/length_1subpath total numOfSubpaths = 58
	40 length-1 subpaths, coverage is 0.689655
	all_subpath/length_2subpath total numOfSubpaths = 91
	22 length-2 subpaths, coverage is 0.241758
	all_subpath/length_3subpath total numOfSubpaths = 179
	16 length-3 subpaths, coverage is 0.0893855
	all_subpath/length_4subpath total numOfSubpaths = 286
	11 length-4 subpaths, coverage is 0.0384615
	all_subpath/length_5subpath total numOfSubpaths = 527
	9 length-5 subpaths, coverage is 0.0170778
	all_subpath/length_6subpath total numOfSubpaths = 890
	7 length-6 subpaths, coverage is 0.00786517
	all_subpath/length_7subpath total numOfSubpaths = 1639
	5 length-7 subpaths, coverage is 0.00305064
	all_subpath/length_8subpath total numOfSubpaths = 2823
	3 length-8 subpaths, coverage is 0.0010627 
	>>>>>>>>finished.
	>>>>>>>>please check file "exec_path.txt" for the execution path and
              and directory "all_subpath" for the subpath coverages from length-1 to length-8.

Now, nCov executes successfully.

**Attention:**

	After compilation the tcas with nCov, there is a file called *tcas.exe* 
	in source directory. It is a instrumented program. You can execute it by
	other input according to the *tcas/script/runall.sh*. 


