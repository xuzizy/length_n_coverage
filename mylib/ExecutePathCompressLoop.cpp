#include <iostream>
#include <sstream>
#include <fstream>  
#include "printSTL.h"
#include "ExecutePath.h"
#include <stack>
#include <cstdlib> 
#include <assert.h>
#include <map>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif


ofstream out("exec_path.txt",ios::app);
ofstream debug("debug.txt",ios::out);
const int MAXNUM=200000000;
PathNode path[MAXNUM];
vector<PathNode*> stk;
int _subpathLength;
int count = 0;

//a map saving loop information.
//key:loop header id
//value: a list of loop exit blocks id
map<int,vector<int> > loopInfo;

void printPath(int);
void init();

int recordBasicBlock(int id,int subpathLength){
/*	static int prevId = 0;
	if(prevId == 22 && id == 78){
		debug<<prevId<<","<<78<<endl<<flush;
		exit(-1);
	}
	prevId = id;*/
   
	static bool firstBlock = true;
	static int loopLength = 0;
	if(firstBlock){

	//	cerr<< "befor init <<"<<endl;
	//	out << -1 << endl;//start tag 
		path[count++] = PathNode(-1 );

		_subpathLength = subpathLength;
		firstBlock = false;
		init();
	}

Begin:
	//printPath(id);

	if(isLoopStart(id )){
		//the same loop, iterator times =+
		if(!stk.empty() && stk.back()->id == id){ 
//			cout << "old loop id :" << id <<". count:"<<count<<". loopLength:"<<loopLength<<endl;
			count = stk.back()->loopStart->index + 1;
		 
			stk.back()->loopStart->loopLength = loopLength;
			stk.back()->loopStart->times++;//loop times++ 
		}else{
		//inner loop started.save the parent loop's length.
			if(!stk.empty()){
				stk.back()->loopStart->loopLength = loopLength;
			}
	//	 	cout << "new loop id :" << id <<". count:"<<count<<". "<<endl;
			 //new loop start,
				path[count] = PathNode(id,true,new LoopStart(count));
				stk.push_back(&path[count]); 
				count++;
			 
		} 
		
		loopLength = 1;  
	}else if(!stk.empty()){	
		//if id is belong to stk.back().exitblocks
		if(isExitBlock(stk.back()->id,id)){
			quitLoop(true,loopLength); 
			goto Begin;
		}
 		
 	//id is in a loop
		//iterator first time
		if(stk.back()->loopStart->times == 0/*||loopLength < stk.back()->loopStart->loopLength*/){
			path[count++] = PathNode(id);
			loopLength++;
		}else//in the same loop,iterator times > 1
			if(path[count].id == id){
				count++;
				loopLength++;
			}else{ //There is a branch in the current loop  
			
				quitLoop(false,loopLength);	// and stk not pop
				goto Begin;
			} 
		 
	}else{//stack is empty,id is nothing to do with any loop.
		path[count++] = PathNode(id ); 
	}

	if(count == MAXNUM){
			savePath();
			static int n_MAXNUM=0;
			count = 0;
			cerr << "path length is "<<(++n_MAXNUM)*MAXNUM<<endl;
		}
		//	static int t = 0;
		 //	if(id == 1195 && t < 1000){
		 		//printPath(id);
		// 		t++;
  	//cerr<<"stack: " << stk<<endl;
		 //	} 
 // 	printPath(id);
	return 0;
}

void init(){
	//read loopHeader and exits block
	ifstream fin("CFG.BE"); 
	int header,exitBlock;
	const int LINELENGTH=1000;
 char line[LINELENGTH];
 while(fin.getline(line,LINELENGTH)){
 	stringstream ss(line);
 	ss >> header;
		while(ss>> exitBlock){ 
			loopInfo[header].push_back(exitBlock);
		}
	}
	//cout  << "loopInfo "<<loopInfo<< endl;
}

//if exitLoop == true,the current loop really exit.
//else there is a branch in the current loop.

inline void quitLoop(bool exitLoop,int& loopLength){
	int startIndex = stk.back()->loopStart->index; 
	int newLoopIndex ;
	//int loopLength = stk.back()->loopStart->loopLength;

 //if stk.back()->loopStart->times
 //copy current iterate path is not wrong.
	if(stk.back()->loopStart->times > 0){
		for(int i = startIndex; i < count; i++){
			path[i+stk.back()->loopStart->loopLength] = path[i];
			// if exitLoop == true,
			//the path[startIndex+loopLength] is not a loop start
			if(i == startIndex){
				if(exitLoop)
					path[startIndex+stk.back()->loopStart->loopLength].isLoopStart = false;
				else{//set new loop's start index 
					newLoopIndex = startIndex+stk.back()->loopStart->loopLength;
					path[newLoopIndex].loopStart->index = i+stk.back()->loopStart->loopLength;
				}
			}
		}
		count += stk.back()->loopStart->loopLength;
	}else 
	//if current loopLength == stk.top loopLength or 
	//stk top's loop times == 0,
	//so current iterator is in stk.top's loop  
 //cif(stk.back()->loopStart->loopLength == loopLength
	//	|| stk.back()->loopStart->times == 0)
	{
		stk.back()->loopStart->times++;
		stk.back()->loopStart->loopLength = loopLength;
	} 

	//update count
	
	//test
//	cout <<"after copy:count = "<<count<<": ";printPath();
	if(exitLoop){ 
		stk.pop_back(); 
		//update loopLength for new stk.pop loopHeader 
		if(!stk.empty()){
			loopLength = count - stk.back()->loopStart->index ;
		}
	}else{
		// Because there is a branch in loop,
		//it should be regarded as a new loop.
		stk.pop_back();
		stk.push_back(&path[newLoopIndex]);
		stk.back()->loopStart->times = 0; 
	}
			
	  
}

inline bool isExitBlock(int loopHeader,int id){
	map<int,vector<int> >::iterator iter = loopInfo.find(loopHeader);
	if(iter == loopInfo.end()){
		cerr<< "error in isExitBlock: can't find loop header "<<loopHeader << endl;
		exit(-1);
	}

	vector<int> &exitBlocks = iter->second;
	for(vector<int>::iterator iter = exitBlocks.begin(); iter != exitBlocks.end(); iter++){
		if(id == *iter)
			return true;
	}

	return false;
}

//stub function
inline bool isLoopStart(int id){

	if(loopInfo.count(id) > 0)
		return true;
	else
		return false;

/*	static pair<int,int> p;
	int prevId;
	if(count > 0){
		prevId = path[count-1].id;
	}else{
		cerr << "error in isLoopStart! Not handle this situation"<<endl;
		exit(-1);
	}

	p = make_pair(prevId,id);
	
		//debug
	if(id == 1195){
		cout << prevId <<" "<<id<<" "<<backEdges.count(p)<<endl; 
	}

//	if (backEdges.count(id))
	if (backEdges.count(p))
		return true;
	else 
		return false;*/
}

/*bool isLoopEnd(int id){
	if(id == 3 || id == 12 || id  == 13)
		return true;
	else
		return false;
}*/    

void printPath(int currentInput){
	cerr << "printPath"<<endl;
	for(int i =0; i < count;i++){
			cerr << path[i] <<" " ;
		}
		cerr <<"("<<currentInput<<")count="<<count<<endl;
}

void savePath(){
	//cerr << "in save path count = " << count <<endl;
	for(int i =0; i < count;i++){
			
			if(path[i].isLoopStart){
				int loopTimes = _subpathLength/path[i].loopStart->loopLength + 2;
				if(loopTimes > path[i].loopStart->times){
					loopTimes = path[i].loopStart->times;
				}
			 printLoop(i,path[i].loopStart->loopLength,loopTimes);
				i += (path[i].loopStart->loopLength-1);
			//	delete path[i].loopStart;
			}else	{
				out << path[i] <<endl;  
			}
		}
		/*for(int i = 0; i < count; i++){
			debug << path[i] ;
			if(path[i].isLoopStart){
				debug << " "<<path[i].loopStart->loopLength<<" "<<path[i].loopStart->times;
			}
			debug << endl;
		}*/

}


void printLoop(int begin,int length,int times){
	for(int t = 0; t < times; t++){
		for(int i = begin,end = begin+length; i < end;i++){
			
			if(i > begin && path[i].isLoopStart){
				int loopTimes = _subpathLength/path[i].loopStart->loopLength + 2;

				if(loopTimes > path[i].loopStart->times){
					loopTimes = path[i].loopStart->times;
				}
			//	out <<_subpathLength<< " true loopTimes " << loopTimes<<endl;;
				printLoop(i,path[i].loopStart->loopLength,loopTimes);
				i += (path[i].loopStart->loopLength-1);
			}else{
				out <<path[i] <<endl;
			}
		}
	}
}



 #ifdef __cplusplus
	}
#endif