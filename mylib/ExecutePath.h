#ifndef EXECUTEPATH_H
#define EXECUTEPATH_H
#include <iostream>
using namespace std;

struct LoopStart{
	int index;
	int loopLength;
	int times;//loop times
	LoopStart(int i = -1,int len = 1,int t = 0):
											index(i),loopLength(len),times(t){
	}

	LoopStart(LoopStart& l){
		index = l.index;
		loopLength = l.loopLength;
		times = l.times;
	}

		friend ostream& operator << (ostream& out, LoopStart& l){
			out << l.index << " "<< l.loopLength << " "<< l.times ;
			return out;
	}
};


struct PathNode{
	int id;
	//int count;
	bool isLoopStart;
	//bool isLoopEnd;
	LoopStart* loopStart;

	PathNode(int i = -1,/*int c,*/bool isStart = false,LoopStart* start=NULL/*,bool end = false*/){
		id = i; 
		isLoopStart = isStart;
		loopStart = start;
		//cerr<< id <<"," << isLoopStart<<"," << loopStart<<endl;
	//	isLoopEnd = false;
	}

	PathNode& operator = (const PathNode& p){
		id = p.id;
		isLoopStart = p.isLoopStart; 
		if(p.isLoopStart == true)
			loopStart = new LoopStart(*(p.loopStart));
		return *this;
	}

	friend ostream& operator << (ostream& out, PathNode& p){
			out << p.id;
		//	if(p.isLoopStart)
			//	out <<"("<<*(p.loopStart)<<")";
			return out;
	}
	friend ostream& operator << (ostream& out, PathNode* p){
			out << *p;
			return out;
	}
};

#ifdef __cplusplus
extern "C" {
#endif

inline bool isLoopStart(int id );

inline bool isExitBlock(int loopHeader,int id);

void quitLoop(bool shouldPop,int &loopLength);
void savePath();
void printLoop(int begin,int length,int times);

#ifdef __cplusplus
}
#endif
#endif