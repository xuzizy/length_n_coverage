#include <iostream>
#include <sstream>
#include <fstream>  
#include "printSTL.h"
#include "ExecutePath.h"
#include <stack>
#include <cstdlib> 
#include <assert.h>
#include <map>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif
/*
recordBasicBlock() and savePath() is instrumented in test code 
to record the execution path.

*/

ofstream out("exec_path.txt",ios::app);
//ofstream debug("debug.txt",ios::out);
const int MAXNUM=1000000;
int path[MAXNUM]; 
int _subpathLength;
int count = 0;
 

void printPath(int);
void init();

int recordBasicBlock(int id,int subpathLength){ 
   
	static bool firstBlock = true; 
	if(firstBlock){ 
		path[count++] = -1; 
		_subpathLength = subpathLength;
		firstBlock = false; 
	}

 	path[count++] = id;

	if(count == MAXNUM){
		savePath();
		static int n_MAXNUM=0;
		count = 0;
		cerr << "path length is "<<(++n_MAXNUM)*MAXNUM<<endl;
	} 
	return 0;
}

  
  

void printPath(int currentInput){
	cerr << "printPath"<<endl;
	for(int i =0; i < count;i++){
			cerr << path[i] <<" " ;
		}
		cerr <<"("<<currentInput<<")count="<<count<<endl;
}

void savePath(){
	//cerr << "in save path count = " << count <<endl;
	for(int i =0; i < count;i++){	 
				out << path[i] <<endl;  
	}
}

 


 #ifdef __cplusplus
	}
#endif