/*
 This file is deprecated. 
 It is not efficient than ExecutePathForSmallCase.cpp.

*/

#include <iostream>
#include <sstream>
#include <fstream>  
#include "../mylib/printSTL.h"
#include <assert.h>
#include <cstdlib>
using namespace std;
 
#ifdef __cplusplus
extern "C" {
#endif

deque<int> execPath;
map<deque<int>,int> subpathMap;
unsigned execPathLen = 0;
unsigned numOfSubpath = 0;  
int _subpathLength=0;
void readSubpath(const char * fileName);

void init(int);

int recordBasicBlock(int block,int subpathLength){ 
 static bool initialized = false;
 if(!initialized){
 	init(subpathLength);
 	initialized = true;
 }  
	// cout << block <<endl; 
	 
	//	debug
 	execPathLen++;
 	if(execPathLen %1000000 == 0){
 		cerr <<execPathLen << endl;
 		cerr<<execPath<<endl;
 	}

		execPath.push_back(block);
		/*
	 *There are some function call can't find which function  it calls.
	 *Such as function pointer,asm language call etc.
	 *So some execution path nodes which actually are covered are not in program CFG.
	 *We have not find perfect solution and just ignore them :)
		*/
		if(subpathMap.count(execPath) > 0){
			subpathMap[execPath]++; 
			//cout << execPath<<endl;
			execPath.pop_front();
		}else if(execPath.size() == subpathLength){//not exist but length is n
			execPath.pop_front();
		}
	 

	return 0;
}

int coverageOutput(const char * fileName){
	ofstream fout(fileName);
	int count = 0;
	if(!fout){
		cerr <<"coverageOutput: unable to open file "<< fileName <<endl;
		exit(-1);
	}
	for(map<deque<int>,int >::iterator it = subpathMap.begin(); 
		it != subpathMap.end(); it++){
		if(it->second){
			count++;
		}
	}
	
	fout << _subpathLength <<"\n";
	fout << numOfSubpath << "\n";
	fout <<count << " length-"<<_subpathLength<<" subpaths, coverage is " << (count*1.0/numOfSubpath)<<endl;
	fout << subpathMap<<"\n";
	fout .close();
	fout.clear();
	return 0;
}

void readSubpath(const char * fileName){
	ifstream fin(fileName);
	if(!fin){
		cerr << "error: unable to open input file "<< fileName<<endl;
		exit(-1);
	}

	unsigned count = 0;
	int subpathLength;
	fin >> subpathLength;
	fin >> numOfSubpath;
	char line[1001];

	fin.getline(line,1000);//read the former line 's  end descriptor.
	cerr << line <<"\n";
	fin.getline(line,1000);//read third line.
 cerr << line << "\n";
	while(fin.getline(line,1000)){
		//cout << line <<" hehe"<<endl;
		stringstream ss(line);
		deque<int> subpath;
		int pathNode;
		while(ss >> pathNode){ 
			if(pathNode >= 0)
				subpath.push_back(pathNode);  
			else{ 
				int coverdCount;
				ss >> coverdCount;
				count++;
				subpathMap[subpath] = coverdCount;
				break;
			}
		}

	 //	cout << "s = " << isCoverd << endl;
		
			 
	} 
		 
	fin.close();
	fin.clear();
	//cerr<<fileName <<" count = " << count << "numOfSubpath = " << numOfSubpath << endl;
	//cerr<< "subpathMap size = " << subpathMap.size() << endl;
	//cerr << subpathMap;
	assert(count == numOfSubpath && count == subpathMap.size());

}

void init(int n){ 
	//for (int i = 1; i <= n; i++){
		stringstream fileName;
		fileName << "all_subpath_online/length_"<<n<<"subpath";
		execPath.clear();
		subpathMap.clear();
		readSubpath(fileName.str().c_str()); 
		_subpathLength = n;
	//	coverageOutput(fileName.str().c_str(),n);
	//}
}

void savePath(){
	stringstream fileName;
	fileName << "all_subpath_online/length_"<<_subpathLength<<"subpath";
	coverageOutput(fileName.str().c_str());
}

 #ifdef __cplusplus
	}
#endif