#include <iostream>
#include <sstream>
#include <fstream>  
#include "../mylib/printSTL.h"
#include <assert.h>
#include <cstdlib>
using namespace std;
 

deque<int> execPath;
map<deque<int>,int> subpathMap;
unsigned execPathLen = 0;
unsigned numOfSubpath = 0; 

void readSubpath();

int readBasicBlock(int n){
	//exec_path.txt contains exection paths of all test cases.
	//each case path is start with # 
	ifstream fin("exec_path.txt");
	if(!fin){
		cerr<<"Failed to open file exec_path.txt"<<endl;
		exit(-1);
	}
	int block;
	execPathLen = 0;
	while(fin >> block){
	// cout << block <<endl;
		//Each run is start with -1
		if(block < 0){
			execPathLen = 0;
		//	cout << endl;
			continue;
		}
	//	cout << block<<",";

		execPathLen ++;
		execPath.push_back(block);
		/*
	 *There are some function call can't find which function  it calls.
	 *Such as function pointer,asm language call etc.
	 *So some execution path nodes which actually are covered are not in program CFG.
	 *We have not find perfect solution and just ignore them :)
		*/
		if(subpathMap.count(execPath) > 0){
			subpathMap[execPath]++; 
			//cout << execPath<<endl;
			execPath.pop_front();
		}else if(execPath.size() == n){//not exist but length is n
			execPath.pop_front();
		}
	}

	return 0;
}

int coverageOutput(const char * fileName,int subpathLength){
	ofstream fout(fileName);
	int count = 0;
	if(!fout){
		cerr <<"error: unable to open file "<< fileName <<endl;
		exit(-1);
	}
	for(map<deque<int>,int >::iterator it = subpathMap.begin(); 
		it != subpathMap.end(); it++){
		if(it->second){
			count++;
		}
	}
	
	fout << subpathLength <<"\n";
	fout << numOfSubpath << "\n";
	fout <<count << " length-"<<subpathLength<<" subpaths, coverage is " << (count*1.0/numOfSubpath)<<endl;
  cout <<count << " length-"<<subpathLength<<" subpaths, coverage is " << (count*1.0/numOfSubpath)<<endl;

	fout << subpathMap<<"\n";
	fout .close();
	fout.clear();
	return 0;
}

void readSubpath(const char * fileName){
	ifstream fin(fileName);
	if(!fin){
		cerr << "error: unable to open input file "<< fileName<<endl;
		exit(-1);
	}

	unsigned count = 0;
	int subpathLength;
	fin >> subpathLength;
	fin >> numOfSubpath;
	char line[1001];

	fin.getline(line,1000);//read the former line 's  end descriptor.
//	cout << line <<"\n";
	fin.getline(line,1000);//read third line.
// cout << line << "\n";
	while(fin.getline(line,1000)){
		//cout << line <<" hehe"<<endl;
		stringstream ss(line);
		deque<int> subpath;
		int pathNode;
		while(ss >> pathNode){ 
			if(pathNode >= 0)
				subpath.push_back(pathNode);  
			else{ 
				int coverdCount;
				ss >> coverdCount;
				count++;
				subpathMap[subpath] = coverdCount;
				break;
			}
		}

	 //	cout << "s = " << isCoverd << endl;
		
			 
	} 
		 
	fin.close();
	fin.clear();
	cout<<fileName <<" total numOfSubpaths = " << numOfSubpath << endl;
	//cout<< "subpathMap size = " << subpathMap.size() << endl;
//	cout << subpathMap;
	assert(count == numOfSubpath && count == subpathMap.size());

}

int main(int argc ,char* argv[]){
	if(argc < 2){
		fprintf(stderr, "Usage: ./coverage n\n");
		exit(-1);
	}
	int n = atoi(argv[1]);
	if( n <= 0){
		fprintf(stderr, "Usage: ./coverage n\n");
		exit(-1);
	}
	for (int i = 1; i <= n; i++){
		stringstream fileName;
		fileName << "all_subpath/length_"<<i<<"subpath";
		execPath.clear();
		subpathMap.clear();
		readSubpath(fileName.str().c_str());
		readBasicBlock(i);
		coverageOutput(fileName.str().c_str(),i);
	}
}